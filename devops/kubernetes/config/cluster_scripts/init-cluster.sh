#!/bin/bash
c=20
e=()
error-counter () {
    if [ $? != 0 ]
    then
        echo "$1 failed"
        e+=($1)
    fi
}

helm repo add appscode https://charts.appscode.com/stable/ 2>> cluster-init.log
error-counter "add appscode"
helm repo add stable https://kubernetes-charts.storage.googleapis.com/ 2>> cluster-init.log
error-counter "add stable" 
helm repo update 2>> cluster-init.log
error-counter "repo update" 

helm install kubedb-controller appscode/kubedb --version v0.12.0 -n kube-system 2>> cluster-init.log
error-counter "kubedb-controller"
helm install kubedb-catalog appscode/kubedb-catalog --version v0.12.0 -n kube-system 2>> cluster-init.log
error-counter "kubedb-catalog"
helm install ingress-contorller -f ./config/helm-ingress-nginx.yml stable/nginx-ingress --version 1.36.0 -n kube-system 2>> cluster-init.log
error-counter "ingress-controller" 

for i in "${e[@]}"
do
    if [ "$i" == "kubedb-catalog" ]
    then
        echo "maybe you need to wait for kubedb to setup"
        sleep 30
        helm install kubedb-catalog appscode/kubedb-catalog --version v0.12.0 -n kube-system 2>> cluster-init.log
        if [ $? == 1 ]
        then
            echo "kubedb-catalog failed again try running:"
            echo "helm install kubedb-catalog appscode/kubedb-catalog --version v0.12.0 -n kube-system"
        else
            c=d
        fi
    fi
    d+=1
done

if [ $c != 20 ]
then
    unset e[$c]
fi
echo "\n \n \n \n"
if [ ${#e[@]} == 0 ]
then
    echo "all is good under the hood"
    rm ./cluster-init.log
else
    echo "${e[@]} have failed check the logs"
    cat cluster-init.log
fi