# Projet Kubernetes  

**Ce projet est un cluster Kubernetes comportant un Wordpress, une base de données ainsi que des outils de monitoring comme Prometheus ou Grafana.**  
 
## 4 Namespaces
  - Kubedb
  - Wordpress
  - Monitoring
  - Registre

### Kubedb

  - Mysql 
  - PhpMyAdmin 

### Wordpress

  - Wordpress

### Monitoring

  - Prometheus
  - Grafana
  
### Registre

  - minio operator
  - minio instance

**Chaque namespace et chaque deploiement possèdent des requirements xxx.**

# Etape 1
>Install kubedb dans le namespace kub-system

```sh
$ helm repo add appscode https://charts.appscode.com/stable/
$ helm repo update
$ helm search appscode/kubedb
$ helm install appscode/kubedb --name kubedb --namespace kube-system
$ helm install appscode/kubedb-catalog --name kubedb-catalog --namespace kube-system
```
>Install ingress dans le namespace kube-system / Ajouter un fichier de config controller ingress

```sh
helm-ingress-nginx dans la racine de ./config
helm install ingress -f config/helm-ingress-nginx.yml stable/nginx-ingres -n kube-system
```

# Etape 2

>Creation des namespaces kind namespace leurs dossier de config respectifs

**Découpage des ressources :**
  - Kubedb      ram=500Mi cpu=0.030
  - Wordpress   ram=90Mi cpu=0.015
  - Monitoring  ram=90Mi cpu=0.015
  - Registry    ram=154Mi cpu=0.005

# Etape 3

>Creation du configmap mysql

```sh
kubectl create configmap mysql-init-script --from-file=./config/kubedb/mysql-init.sql
```