# Exercices MongoDB

db.getCollection('restaurant').find({})

db.restaurant.find({},{"restaurant_id":1, "name":1, "borough":1, "cuisine":1})

db.restaurant.find({},{"restaurant_id":1, "name":1, "borough":1, "cuisine":1, "_id":0})

db.restaurant.find({},{"restaurant_id":1, "name":1, "borough":1, "cuisine":1, "address.zipcode":1, "_id":0})

db.restaurant.find({"borough":"Bronx"})

db.restaurant.find({"borough":"Bronx"}).limit(5)

db.restaurant.find({"borough":"Bronx"}).skip(5).limit(5)

db.restaurant.find({"grades.score": { $gt: 90 }})

db.restaurant.find({"grades.score": { $gt: 80, $lte: 100 }})

db.restaurant.find({"address.coord.0": { $lt: -95.754168 }})

db.restaurant.find({"cuisine": { $ne:"American" },"grades.score": { $gt: 70 }, "address.coord.0": { $lt: -65.754168 }})

db.restaurant.find({$and: [{"cuisine": { $ne:"American"}},{"grades.score": { $gt: 70 } },{"address.coord.0": { $lt: -65.754168 }}]})

db.restaurant.find({$and: [{"cuisine": { $ne:"American"}},{"grades.grade": "A" },{"borough": { $ne:"Brooklyn" }}]}).sort({"cuisine":1})

db.restaurant.find({name: /^Wil/},{"_id":1, "name":1, "borough":1, "cuisine":1})

db.restaurant.find({name: /ces$/},{"_id":1, "name":1, "borough":1, "cuisine":1})*

db.restaurant.find({name: /.*Reg.*/},{"_id":1, "name":1, "borough":1, "cuisine":1})

db.restaurant.find({"borough":"Bronx",$or: [{"cuisine": "American"},{"cuisine": "Chinese"}]})

db.restaurant.find({"borough": {$in: [ "Staten Island", "Brooklyn", "Bronx" ]}},{"_id":1, "name":1, "borough":1, "cuisine":1})

db.restaurant.find({"borough": {$nin: [ "Staten Island", "Brooklyn", "Bronx" ]}},{"_id":1, "name":1, "borough":1, "cuisine":1})

db.restaurant.find({"grades.score": {$not: {$gt : 10}}},{"_id":1, "name":1, "borough":1, "cuisine":1 })

db.restaurant.find({$or: [{name: /^Wil/}, {"$and": [{"cuisine" : {$ne :"American "}}, {"cuisine" : {$ne :"Chinese"}}]}]},{"id" : 1,"name":1,"borough":1,"cuisine" :1})

db.restaurant.find({"grades.grade": "A","grades.score": 11,"grades.date": ISODate("2014-08-11T00:00:00Z")},{"_id": 1,"name":1,"grades":1})

db.restaurant.find({"grades.1.grade": "A","grades.1.score": 9,"grades.1.date": ISODate("2014-08-11T00:00:00Z")},{"_id": 1,"name":1,"grades":1})
db.restaurant.find({"address.coord.1": { $gt: 42, $lte: 52 }},{"_id": 1,"name": 1,"address.coord":1})
db.restaurant.insert(
               [{
        "address" : {
            "building" : "45", 
            "coord" : [-73.856012, 40.848412], 
            "street" : "45 Faubourg Lacapelle", 
            "zipcode" : "82000"}, 
            "borough" : "Montauban", 
            "cuisine" : "Française", 
            "grades" : [
                    {"date" : ISODate("2019-03-03T00:00:00.000+0000"), 
                    "grade" : "A", 
                    "score" : NumberInt(12)}, 
                    {"date" : ISODate("2018-09-11T00:00:00.000+0000"), 
                    "grade" : 
                    "A", "score" : NumberInt(12)}, 
                    {"date" : ISODate("2017-01-24T00:00:00.000+0000"), 
                    "grade" : "A", 
                    "score" : NumberInt(12)}], 
            "name" : "Le passage du 45", 
            "restaurant_id" : "1"},
               {
        "address" : {
            "building" : "12", 
            "coord" : [-70.006077, 40.008447], 
            "street" : "Ghetto", 
            "zipcode" : "82000"}, 
            "borough" : "Montauban", 
            "cuisine" : "Libanaise", 
            "grades" : [
                    {"date" : ISODate("2019-03-03T00:00:00.000+0000"), 
                    "grade" : "B", 
                    "score" : NumberInt(12)}, 
                    {"date" : ISODate("2012-09-11T00:00:00.000+0000"), 
                    "grade" : "B", 
                    "score" : NumberInt(12)}, 
                    {"date" : ISODate("2012-01-24T00:00:00.000+0000"), 
                    "grade" : "B", 
                    "score" : NumberInt(12)}], 
            "name" : "O bled Custom", 
            "restaurant_id" : "2"},
                {
        "address" : {
            "building" : "70", 
            "coord" : [-73.856077, 40.848447], 
            "street" : "Chez moi", 
            "zipcode" : "82000"}, 
            "borough" : "Montauban", 
            "cuisine" : "Française", 
            "grades" : [
                    {"date" : ISODate("2019-03-03T00:00:00.000+0000"), 
                    "grade" : "C", 
                    "score" : NumberInt(12)}, 
                    {"date" : ISODate("2018-09-11T00:00:00.000+0000"), 
                    "grade" : "c", 
                    "score" : NumberInt(12)}, 
                    {"date" : ISODate("2017-01-24T00:00:00.000+0000"), 
                    "grade" : "C", 
                    "score" : NumberInt(12)}], 
            "name" : "My at home", 
            "restaurant_id" : "3"},
                {
        "address" : {
            "building" : "45", 
            "coord" : [-73.856077, 40.848447], 
            "street" : "Quai Chartron", 
            "zipcode" : "33000"}, 
            "borough" : "Bordeaux", 
            "cuisine" : "Mexicaine", 
            "grades" : [
                    {"date" : ISODate("2019-03-03T00:00:00.000+0000"), 
                    "grade" : "A", 
                    "score" : NumberInt(12)}, 
                    {"date" : ISODate("2018-09-11T00:00:00.000+0000"), 
                    "grade" : "A", 
                    "score" : NumberInt(12)}, 
                    {"date" : ISODate("2017-01-24T00:00:00.000+0000"), 
                    "grade" : "A", 
                    "score" : NumberInt(12)}], 
            "name" : "Cofete.B", 
            "restaurant_id" : "4"},
               {
        "address" : {
            "building" : "5", 
            "coord" : [-77.000000, 43.668447], 
            "street" : "Place du capitole", 
            "zipcode" : "33000"}, 
            "borough" : "Toulouse", 
            "cuisine" : "Tout", 
            "grades" : [
                    {"date" : ISODate("2019-03-03T00:00:00.000+0000"), 
                    "grade" : "S", 
                    "score" : NumberInt(12)}, 
                    {"date" : ISODate("2018-09-11T00:00:00.000+0000"), 
                    "grade" : "S", 
                    "score" : NumberInt(12)}, 
                    {"date" : ISODate("2017-01-24T00:00:00.000+0000"), 
                    "grade" : "S", 
                    "score" : NumberInt(12)}], 
            "name" : "Le cinq", 
            "restaurant_id" : "5"}]
                    );
db.restaurant.find({"borough": {$in: [ "Montauban","Bordeaux","Toulouse" ]}})

db.restaurant.update([{"_id" : "5db58551556d53d9794d6c0f"},{$set:{"grades":[{"date":ISODate("2799-03-03T00:00:00.000+0000"),"grade":"S","score":9999},{"date":ISODate("2799-03-03T00:00:00.000+0000"),"grade":"S","score":9999}]}}])

db.restaurant.remove({"borough": "Staten Island"})

db.restaurant.updateMany({"borough": "Manhattan"}, { $rename: { "borough": "Brooklyn"}})

db.restaurant.distinct("borough")

db.restaurant

db.restaurants.aggregate(
        [
        { 
            $match: {
                cuisine:  {$in:["Chinese", "Irish", "Japanese", "Indian", "American", "Hamburgers"]}
            }
        },
        {
             $project: { 
                 name: 1,
                 restaurant_id: { $multiply: [{$toInt: "$restaurant_id"}, 100]},
                 "borough" : {$concat: ["$borough", "/ New York"]}
                 }
        }
        ]
    );
    

db.retaurant.aggregate(
        [
                { $group : {"_id" : "$borough", "total" : {$sum : 1} } }
        ]
)

varSort = { $sort : {"name":1} };
db.restaurant.aggregate( [ varMatch, varProject, varSort ] ); 
varGroup3 = { $group : {"_id" : "$borough", "total" : {$sum : 1} } };
db.restaurant.aggregate( [ varMatch, varGroup3 ] );