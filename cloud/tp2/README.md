# I. Gestion de conteneurs Docker
- 🌞
```
[root@docker tp2]# ps -e | grep -i -e 'containerd' -e 'dockerd'
107975 ?        00:00:04 containerd
107976 ?        00:00:01 dockerd
108208 ?        00:00:00 containerd-shim
108280 ?        00:00:00 containerd-shim
```
- 🌞

editer le ficher ``/lib/systemd/system/docker.service``
ajouter un nouveau endpoint en modifiant la ligne ``ExecStart=/usr/bin/dockerd -H fd:// -H=tcp://0.0.0.0:5555``
redémarrer docker et les services avec ``sudo systemctl daemon-reload && sudo systemctl restart docker``
liste des containers et images
```
[root@docker tp2]# curl 172.17.0.1:5555/images/json 
[{"Containers":-1,"Created":1579474933,"Id":"sha256:d9878040f92e15063cd202cb64441b920b0872530a3439167896998ed448c43d","Labels":null,"ParentId":"sha256:4317cb12443472edc8506d152ad243825395aa0327962a48dd74634d464d0d1a","RepoDigests":null,"RepoTags":["tp1_python-app:latest"],"SharedSize":-1,"Size":120342676,"VirtualSize":120342676},{"Containers":-1,"Created":1579474916,"Id":"sha256:f234eac78947523411d4f095655d748a7a761c2e27bff05044ea011332d2dfd0","Labels":{},"ParentId":"sha256:2f5c1b382508e53ce30a01ad498dd21e134e13eeb9ca8e3b24425af7a9d42009","RepoDigests":null,"RepoTags":["tp1_nginx:latest"],"SharedSize":-1,"Size":54045092,"VirtualSize":54045092},{"Containers":-1,"Created":1579474916,"Id":"sha256:e9101d068bf7272f2daeb8a10988e2abe0897e166692ee425667aa14e970a9db","Labels":null,"ParentId":"sha256:c2fb64e66bed7ac8a4942da7e66ae3eef141b81b2ad18ddbf56a276fc79ff8c7","RepoDigests":null,"RepoTags":["python:alpine"],"SharedSize":-1,"Size":120342676,"VirtualSize":120342676},{"Containers":-1,"Created":1579474891,"Id":"sha256:120ed284a5b192713c9dee1fa3ba134a64615a7ce21300fc166ae050a6bb0b30","Labels":{},"ParentId":"sha256:c7cb30ba022575ea32340a3a027d6203e5b780784e0cfe6f8fb08718a870f7ef","RepoDigests":null,"RepoTags":["nginx:1.17.7-alpine"],"SharedSize":-1,"Size":54041513,"VirtualSize":54041513},{"Containers":-1,"Created":1579473207,"Id":"sha256:252376fbfd5fe7e7e5011b5234af7de2d0e01463912369f0519b8d42b8f50ba4","Labels":{},"ParentId":"sha256:f2cfe739e4eb7fd7a6442eabe4496dc6842163efe92db00de87127544f4d2344","RepoDigests":null,"RepoTags":["nginx:1.17.7"],"SharedSize":-1,"Size":54037934,"VirtualSize":54037934},{"Containers":-1,"Created":1579473198,"Id":"sha256:15bac1d253035e9fa9c31ea5664df8b2f05fbd3d8eb6b1fa4a741889ab196ba6","Labels":{},"ParentId":"sha256:221645c09ed0cabe8dfc3d0c24ace6be99f6804c30ee93fe7c717ba6c4db7d7e","RepoDigests":null,"RepoTags":["nginx_server:test"],"SharedSize":-1,"Size":54037934,"VirtualSize":54037934},{"Containers":-1,"Created":1579472305,"Id":"sha256:d6f0940f6ba0a31ca6edd358a61dbdfdcffd91c920eb708da3cd183bbe54241f","Labels":null,"ParentId":"sha256:34fc6a00423ae90b8e445ccc027546b1d0c69b5d3de6ce94d5e9ea7bb929b0f9","RepoDigests":null,"RepoTags":["python:test"],"SharedSize":-1,"Size":120358299,"VirtualSize":120358299},{"Containers":-1,"Created":1579472065,"Id":"sha256:3bb6793e46b7aaa9c5b2579c004b34288dcac92f8f2143491105e307254a091b","Labels":{},"ParentId":"sha256:ae638eaa276310d0df64948e75630469c8a0d998bc391c8d09309ef11599db81","RepoDigests":["<none>@<none>"],"RepoTags":["<none>:<none>"],"SharedSize":-1,"Size":54037934,"VirtualSize":54037934},{"Containers":-1,"Created":1579471283,"Id":"sha256:627d23006c25a001fddec4ad6e1d3360f6c92acaa0b3d7e0f4a52c6fd8bee39e","Labels":{},"ParentId":"sha256:9cb42e7fc14fe6f0a5542d78c7333a7ef534860fb16f1a8184c704021eee0e0c","RepoDigests":["<none>@<none>"],"RepoTags":["<none>:<none>"],"SharedSize":-1,"Size":54037934,"VirtualSize":54037934},{"Containers":-1,"Created":1579468651,"Id":"sha256:cb5b47edc9ac9b099615d70163a9e65916c1e39edd89df208a0f0be63b2d1fcc","Labels":null,"ParentId":"sha256:f80e44e2fc01d3db9a9b62054526923b029c9e05d2009f002d1ed40e594768f5","RepoDigests":null,"RepoTags":["python_server:latest","python_server:working"],"SharedSize":-1,"Size":112445210,"VirtualSize":112445210},{"Containers":-1,"Created":1579321066,"Id":"sha256:b68707e68547e636f2544e9283f02beed46d536f644573c8b35c368f9abbe078","Labels":null,"ParentId":"","RepoDigests":["redis@sha256:cb9783b1c39bb34f8d6572406139ab325c4fac0b28aaa25d5350495637bb2f76"],"RepoTags":["redis:alpine"],"SharedSize":-1,"Size":29780132,"VirtualSize":29780132},{"Containers":-1,"Created":1579320988,"Id":"sha256:6e78e36b108877d0f79b594fbdd8bfb6f1855883d00d83f793f401e7fcf047fb","Labels":null,"ParentId":"","RepoDigests":["redis@sha256:a0a282c2699db44b0b6253ac8c31d0926cc2f87403aae798e93472a8edc3d04c"],"RepoTags":["redis:rc-alpine3.11"],"SharedSize":-1,"Size":31403497,"VirtualSize":31403497},{"Containers":-1,"Created":1579315404,"Id":"sha256:0fbb5099adea870240d7dc04057d957a45695f5396562bd604f48043daf87bf3","Labels":null,"ParentId":"","RepoDigests":["python@sha256:3daada4370b67a869cd52febc126261a354e2ee100524f712876b519b052f387"],"RepoTags":null,"SharedSize":-1,"Size":109078806,"VirtualSize":109078806},{"Containers":-1,"Created":1578975871,"Id":"sha256:e5f879046af2ded2382798f2e6a8b3fc9ad3b02e8aed31e4eb9a756505e80422","Labels":{},"ParentId":"sha256:3ccc3d6eff4eafcc1a3b5d7f175412e635c27465f298c62f7c7d38144254a371","RepoDigests":null,"RepoTags":["nginx:latest"],"SharedSize":-1,"Size":54037934,"VirtualSize":54037934},{"Containers":-1,"Created":1578975861,"Id":"sha256:d77f7c75eed61a1abc0cce0cca886bf495ae22f956a20476dff9d073ce75edab","Labels":{},"ParentId":"sha256:26e26e347ebbc9b38ea84bb748218ecba37bb96ee809acecbd29bb03a573371e","RepoDigests":null,"RepoTags":["nginx:mine"],"SharedSize":-1,"Size":54037934,"VirtualSize":54037934},{"Containers":-1,"Created":1578092796,"Id":"sha256:4c30403e92a1a0d293dd6605d669f068610e8a2e0785132107241cc315758370","Labels":null,"ParentId":"","RepoDigests":["python@sha256:23f6b2c7c94054a451965e37932716e36b8630461ed635a666f100c441798110"],"RepoTags":null,"SharedSize":-1,"Size":112443790,"VirtualSize":112443790},{"Containers":-1,"Created":1577215212,"Id":"sha256:cc0abc535e36a7ede71978ba2bbd8159b8a5420b91f2fbc520cdf5f673640a34","Labels":null,"ParentId":"","RepoDigests":["alpine@sha256:2171658620155679240babee0a7714f6509fae66898db422ad803b951257db78"],"RepoTags":["alpine:3.11","alpine:latest"],"SharedSize":-1,"Size":5591300,"VirtualSize":5591300},{"Containers":-1,"Created":1516317158,"Id":"sha256:e06c3dbbfe239c6fca50b6ab6935b3122930fa2eea2136979e5b46ad77ecb685","Labels":null,"ParentId":"","RepoDigests":["httpd@sha256:9784d70c8ea466fabd52b0bc8cde84980324f9612380d22fbad2151df9a430eb"],"RepoTags":["httpd:2.2"],"SharedSize":-1,"Size":171293537,"VirtualSize":171293537},{"Containers":-1,"Created":1482866392,"Id":"sha256:966594183fba7c01c2e65aa5eadb79cf3e0a7ccfd8ac9a8cb6a58e9020e23d9a","Labels":{},"ParentId":"","RepoDigests":["nginx@sha256:ce50816e7216a66ff1e0d99e7d74891c4019952c9e38c690b3c5407f7af57555"],"RepoTags":["nginx:1.10.2-alpine"],"SharedSize":-1,"Size":54034355,"VirtualSize":54034355}]
```

```
[root@docker tp2]# curl 172.17.0.1:5555/containers/json
[{"Id":"fcaf7050af8ac19e4bb982eb6e89211569ac887a91d34a5c2456d0f6df2c07d8","Names":["/tp1_python-app_1"],"Image":"tp1_python-app","ImageID":"sha256:d9878040f92e15063cd202cb64441b920b0872530a3439167896998ed448c43d","Command":"python app.py","Created":1579474943,"Ports":[{"IP":"0.0.0.0","PrivatePort":8888,"PublicPort":8888,"Type":"tcp"}],"Labels":{"com.docker.compose.config-hash":"fe400f9e834da8dbc3ca404bdb4f3cd9b4a4e9de62e9e7925efccab6764e5016","com.docker.compose.container-number":"1","com.docker.compose.oneoff":"False","com.docker.compose.project":"tp1","com.docker.compose.project.config_files":"docker-compose.yml","com.docker.compose.project.working_dir":"/root/cloud-b3-2019-master/tp/tp1","com.docker.compose.service":"python-app","com.docker.compose.version":"1.25.0"},"State":"running","Status":"Up 20 minutes","HostConfig":{"NetworkMode":"tp1_compose-net"},"NetworkSettings":{"Networks":{"tp1_compose-net":{"IPAMConfig":null,"Links":null,"Aliases":null,"NetworkID":"b680a13064291aabf770f39d4bc1c7ac720d5a69c5c86a57a6a9435db61e43b8","EndpointID":"547bccbc7d2fecd5486dd1778d4d5779ab18f8c6300406635895ceedd7f9b3eb","Gateway":"172.20.0.1","IPAddress":"172.20.0.2","IPPrefixLen":16,"IPv6Gateway":"","GlobalIPv6Address":"","GlobalIPv6PrefixLen":0,"MacAddress":"02:42:ac:14:00:02","DriverOpts":null}}},"Mounts":[{"Type":"bind","Source":"/root/cloud-b3-2019-master/tp/tp1/python-app","Destination":"/python-app","Mode":"rw","RW":true,"Propagation":"rprivate"}]},{"Id":"a392705d5bf60e18e28a49696245be8a80fa648d54a5d7f13c4847908a235261","Names":["/tp1_nginx_1"],"Image":"tp1_nginx","ImageID":"sha256:f234eac78947523411d4f095655d748a7a761c2e27bff05044ea011332d2dfd0","Command":"nginx","Created":1579474916,"Ports":[],"Labels":{"com.docker.compose.config-hash":"c476dc08db4b7bb69c0e360575200e84799eb625b8854b32cc39f60c77e4a572","com.docker.compose.container-number":"1","com.docker.compose.oneoff":"False","com.docker.compose.project":"tp1","com.docker.compose.project.config_files":"docker-compose.yml","com.docker.compose.project.working_dir":"/root/cloud-b3-2019-master/tp/tp1","com.docker.compose.service":"nginx","com.docker.compose.version":"1.25.0"},"State":"restarting","Status":"Restarting (0) 2 seconds ago","HostConfig":{"NetworkMode":"tp1_compose-net"},"NetworkSettings":{"Networks":{"tp1_compose-net":{"IPAMConfig":null,"Links":null,"Aliases":null,"NetworkID":"b680a13064291aabf770f39d4bc1c7ac720d5a69c5c86a57a6a9435db61e43b8","EndpointID":"","Gateway":"","IPAddress":"","IPPrefixLen":0,"IPv6Gateway":"","GlobalIPv6Address":"","GlobalIPv6PrefixLen":0,"MacAddress":"","DriverOpts":null}}},"Mounts":[]}]
```

# II. Sandboxing

## A. Exploration manuelle
- 🌞
trouver PID de bash
```
[root@docker tp2]# ps -e | grep -i bash
8540 tty1     00:00:00 bash
9009 ?        00:00:00 bash
9365 pts/0    00:00:00 bash
```
faire appel au kernel pour avoir les namespaces
```
[root@docker tp2]# ls -al /proc/8540/ns
total 0
dr-x--x--x. 2 root root 0 Jan 26 18:33 .
dr-xr-xr-x. 9 root root 0 Jan 26 18:24 ..
lrwxrwxrwx. 1 root root 0 Jan 26 19:27 cgroup -> 'cgroup:[4026531835]'
lrwxrwxrwx. 1 root root 0 Jan 26 19:27 ipc -> 'ipc:[4026531839]'
lrwxrwxrwx. 1 root root 0 Jan 26 19:27 mnt -> 'mnt:[4026531840]'
lrwxrwxrwx. 1 root root 0 Jan 26 19:27 net -> 'net:[4026531992]'
lrwxrwxrwx. 1 root root 0 Jan 26 18:33 pid -> 'pid:[4026531836]'
lrwxrwxrwx. 1 root root 0 Jan 26 19:32 pid_for_children -> 'pid:[4026531836]'
lrwxrwxrwx. 1 root root 0 Jan 26 19:27 user -> 'user:[4026531837]'
lrwxrwxrwx. 1 root root 0 Jan 26 19:27 uts -> 'uts:[4026531838]'
```
## B. unshare
- 🌞
```
[root@docker tp2]# unshare bash -u -p -n -mbash-4.4# hostname
bash-4.4# hostnamectl get hostname
bash-4.4# ps
bash-4.4#
```
- 🌞
```
[root@docker tp2]# unshare bash -u -p -n -mbash-4.4# hostname
bash-4.4# hostnamectl get hostname
bash-4.4# ps
bash-4.4#
```
- 🌞
```
[root@docker tp2]# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
705fd1b52b6b        debian              "sleep 99999"       3 minutes ago       Up 3 minutes                            naughty_goldstine
[root@docker tp2]# docker top 705fd1b52b6b
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                122215              122199              0                   20:22               ?                   00:00:00            sleep 99999
[root@docker tp2]# ls -al /proc/122215/ns
total 0
dr-x--x--x. 2 root root 0 Jan 26 20:22 .
dr-xr-xr-x. 9 root root 0 Jan 26 20:22 ..
lrwxrwxrwx. 1 root root 0 Jan 26 20:24 cgroup -> 'cgroup:[4026531835]'
lrwxrwxrwx. 1 root root 0 Jan 26 20:24 ipc -> 'ipc:[4026532612]'
lrwxrwxrwx. 1 root root 0 Jan 26 20:24 mnt -> 'mnt:[4026532610]'
lrwxrwxrwx. 1 root root 0 Jan 26 20:22 net -> 'net:[4026532618]'
lrwxrwxrwx. 1 root root 0 Jan 26 20:24 pid -> 'pid:[4026532613]'
lrwxrwxrwx. 1 root root 0 Jan 26 20:24 pid_for_children -> 'pid:[4026532613]'
lrwxrwxrwx. 1 root root 0 Jan 26 20:24 user -> 'user:[4026531837]'
lrwxrwxrwx. 1 root root 0 Jan 26 20:24 uts -> 'uts:[4026532611]'
```
# D. nsenter
``[root@docker tp2]# sudo nsenter -t $(docker inspect --format '{{ .State.Pid }}' $(docker ps -lq)) -m -u -i -n -p -w``
```
[root@docker tp2]# sudo nsenter -t $(docker inspect --format '{{ .State.Pid }}' $(docker ps -lq)) -m -u -i -n -p -w
mesg: ttyname failed: No such device
root@705fd1b52b6b:/# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
269: eth0@if270: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
mesg: ttyname failed: No such device
269: eth0@if270: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```
``docker top $(docker ps -lq)``
```
[root@docker tp2]# docker top $(docker ps -lq)
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                122215              122199              0                   20:22               ?                   00:00:00            sleep 99999
```
```
[root@docker tp2]# cat /proc/mounts
sysfs /sys sysfs rw,seclabel,nosuid,nodev,noexec,relatime 0 0
proc /proc proc rw,nosuid,nodev,noexec,relatime 0 0
devtmpfs /dev devtmpfs rw,seclabel,nosuid,size=909960k,nr_inodes=227490,mode=755 0 0
securityfs /sys/kernel/security securityfs rw,nosuid,nodev,noexec,relatime 0 0
tmpfs /dev/shm tmpfs rw,seclabel,nosuid,nodev 0 0
devpts /dev/pts devpts rw,seclabel,nosuid,noexec,relatime,gid=5,mode=620,ptmxmode=000 0 0
tmpfs /run tmpfs rw,seclabel,nosuid,nodev,mode=755 0 0
tmpfs /sys/fs/cgroup tmpfs ro,seclabel,nosuid,nodev,noexec,mode=755 0 0
cgroup /sys/fs/cgroup/systemd cgroup rw,seclabel,nosuid,nodev,noexec,relatime,xattr,release_agent=/usr/lib/systemd/systemd-cgroups-agent,name=systemd 0 0
pstore /sys/fs/pstore pstore rw,seclabel,nosuid,nodev,noexec,relatime 0 0
bpf /sys/fs/bpf bpf rw,nosuid,nodev,noexec,relatime,mode=700 0 0
cgroup /sys/fs/cgroup/freezer cgroup rw,seclabel,nosuid,nodev,noexec,relatime,freezer 0 0
cgroup /sys/fs/cgroup/blkio cgroup rw,seclabel,nosuid,nodev,noexec,relatime,blkio 0 0
cgroup /sys/fs/cgroup/net_cls,net_prio cgroup rw,seclabel,nosuid,nodev,noexec,relatime,net_cls,net_prio 0 0
cgroup /sys/fs/cgroup/pids cgroup rw,seclabel,nosuid,nodev,noexec,relatime,pids 0 0
cgroup /sys/fs/cgroup/perf_event cgroup rw,seclabel,nosuid,nodev,noexec,relatime,perf_event 0 0
cgroup /sys/fs/cgroup/cpuset cgroup rw,seclabel,nosuid,nodev,noexec,relatime,cpuset 0 0
cgroup /sys/fs/cgroup/devices cgroup rw,seclabel,nosuid,nodev,noexec,relatime,devices 0 0
cgroup /sys/fs/cgroup/cpu,cpuacct cgroup rw,seclabel,nosuid,nodev,noexec,relatime,cpu,cpuacct 0 0
cgroup /sys/fs/cgroup/hugetlb cgroup rw,seclabel,nosuid,nodev,noexec,relatime,hugetlb 0 0
cgroup /sys/fs/cgroup/memory cgroup rw,seclabel,nosuid,nodev,noexec,relatime,memory 0 0
cgroup /sys/fs/cgroup/rdma cgroup rw,seclabel,nosuid,nodev,noexec,relatime,rdma 0 0
configfs /sys/kernel/config configfs rw,relatime 0 0
/dev/mapper/cl-root / xfs rw,seclabel,relatime,attr2,inode64,noquota 0 0
selinuxfs /sys/fs/selinux selinuxfs rw,relatime 0 0
systemd-1 /proc/sys/fs/binfmt_misc autofs rw,relatime,fd=39,pgrp=1,timeout=0,minproto=5,maxproto=5,direct,pipe_ino=21595 0 0
mqueue /dev/mqueue mqueue rw,seclabel,relatime 0 0
debugfs /sys/kernel/debug debugfs rw,seclabel,relatime 0 0
hugetlbfs /dev/hugepages hugetlbfs rw,seclabel,relatime,pagesize=2M 0 0
/dev/sda1 /boot ext4 rw,seclabel,relatime 0 0
tmpfs /run/user/0 tmpfs rw,seclabel,nosuid,nodev,relatime,size=184944k,mode=700 0 0
overlay /var/lib/docker/overlay2/594057ef17bfb62ba1c48ae776f89f1e64a48bef4ed3a0d08be39fe2fbd0e967/merged overlay rw,seclabel,relatime,lowerdir=/var/lib/docker/overlay2/l/TDWUW5VDPIP2MY4JSFRBFMTYOB:/var/lib/docker/overlay2/l/IRMVGUNEX4NXMFFH5ZXGVM5IY5,upperdir=/var/lib/docker/overlay2/594057ef17bfb62ba1c48ae776f89f1e64a48bef4ed3a0d08be39fe2fbd0e967/diff,workdir=/var/lib/docker/overlay2/594057ef17bfb62ba1c48ae776f89f1e64a48bef4ed3a0d08be39fe2fbd0e967/work 0 0
shm /var/lib/docker/containers/705fd1b52b6b62ad073081278eb4c65fbae9e6225cdff703438145ec6b8c2ca5/mounts/shm tmpfs rw,seclabel,nosuid,nodev,noexec,relatime,size=65536k 0 0
nsfs /run/docker/netns/0f2d597a2a69 nsfs rw,seclabel 0 0
binfmt_misc /proc/sys/fs/binfmt_misc binfmt_misc rw,relatime 0 0
```
## E. Et alors, les namespaces User ?
``sudo dockerd --userns-remap=isolated & ``
```
[root@docker tp2]# systemctl stop docker
[root@docker tp2]# sudo dockerd --userns-remap=isolated & 
[1] 123001
[root@docker tp2]# INFO[2020-01-26T21:05:34.416542460+01:00] User namespaces: ID ranges will be mapped to subuid/subgid ranges of: isolated:isolated 
INFO[2020-01-26T21:05:34.425797471+01:00] User namespaces: ID ranges will be mapped to subuid/subgid ranges of: isolated:isolated 
INFO[2020-01-26T21:05:34.427240225+01:00] parsed scheme: "unix"                         module=grpc
INFO[2020-01-26T21:05:34.427264241+01:00] scheme "unix" not registered, fallback to default scheme  module=grpc
INFO[2020-01-26T21:05:34.427361420+01:00] parsed scheme: "unix"                         module=grpc
INFO[2020-01-26T21:05:34.427368685+01:00] scheme "unix" not registered, fallback to default scheme  module=grpc
INFO[2020-01-26T21:05:34.428099806+01:00] ccResolverWrapper: sending new addresses to cc: [{unix:///run/containerd/containerd.sock 0  <nil>}]  module=grpc
INFO[2020-01-26T21:05:34.428128244+01:00] ClientConn switching balancer to "pick_first"  module=grpc
INFO[2020-01-26T21:05:34.428180607+01:00] pickfirstBalancer: HandleSubConnStateChange: 0xc4205e74c0, CONNECTING  module=grpc
INFO[2020-01-26T21:05:34.428654979+01:00] pickfirstBalancer: HandleSubConnStateChange: 0xc4205e74c0, READY  module=grpc
INFO[2020-01-26T21:05:34.431299800+01:00] ccResolverWrapper: sending new addresses to cc: [{unix:///run/containerd/containerd.sock 0  <nil>}]  module=grpc
INFO[2020-01-26T21:05:34.431340088+01:00] ClientConn switching balancer to "pick_first"  module=grpc
INFO[2020-01-26T21:05:34.431375934+01:00] pickfirstBalancer: HandleSubConnStateChange: 0xc4205e77b0, CONNECTING  module=grpc
INFO[2020-01-26T21:05:34.431542903+01:00] pickfirstBalancer: HandleSubConnStateChange: 0xc4205e77b0, READY  module=grpc
INFO[2020-01-26T21:05:34.442353268+01:00] Graph migration to content-addressability took 0.00 seconds 
WARN[2020-01-26T21:05:34.442882787+01:00] Your kernel does not support cgroup blkio weight 
WARN[2020-01-26T21:05:34.442926159+01:00] Your kernel does not support cgroup blkio weight_device 
INFO[2020-01-26T21:05:34.443550988+01:00] Loading containers: start.                   
INFO[2020-01-26T21:05:34.810582534+01:00] Default bridge (docker0) is assigned with an IP address 172.17.0.0/16. Daemon option --bip can be used to set a preferred IP address 
INFO[2020-01-26T21:05:34.997739651+01:00] Loading containers: done.                    
INFO[2020-01-26T21:05:35.024155594+01:00] Docker daemon                                 commit=4c52b90 graphdriver(s)=overlay2 version=18.09.1
INFO[2020-01-26T21:05:35.024342276+01:00] Daemon has completed initialization          
INFO[2020-01-26T21:05:35.035212062+01:00] API listen on /var/run/docker.sock  
```
## F. Isolation réseau ?

- 🌞
```
[root@docker tp2]# docker run -d -p 8888:7777 debian sleep 99999
3eb8af516d8cf52d07971ab63f8a847361a03e150c2fb6a0b4acf68505018e70
```
- 🌞
```
docker exec -it  $(docker ps -lq) sh 
# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
277: eth0@if278: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:03 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.3/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```

- 🌞

    -
    ```
    [root@docker tp2]# ip a | grep docker
    6: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
        inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
    270: veth2bbe464@if269: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default 
    278: veth5d679c3@if277: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default 
    ```
    -
    ```
    [root@docker tp2]# ip a | grep veth
    270: veth2bbe464@if269: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default 
    278: veth5d679c3@if277: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default 
    ```
    -
    ```
    [root@docker tp2]# iptables -L
    Chain INPUT (policy ACCEPT)
    target     prot opt source               destination         

    Chain FORWARD (policy ACCEPT)
    target     prot opt source               destination         
    DOCKER-USER  all  --  anywhere             anywhere            
    DOCKER-ISOLATION-STAGE-1  all  --  anywhere             anywhere            
    ACCEPT     all  --  anywhere             anywhere             ctstate RELATED,ESTABLISHED
    DOCKER     all  --  anywhere             anywhere            
    ACCEPT     all  --  anywhere             anywhere            
    ACCEPT     all  --  anywhere             anywhere            
    ACCEPT     all  --  anywhere             anywhere             ctstate RELATED,ESTABLISHED
    DOCKER     all  --  anywhere             anywhere            
    ACCEPT     all  --  anywhere             anywhere            
    ACCEPT     all  --  anywhere             anywhere            
    ACCEPT     all  --  anywhere             anywhere             ctstate RELATED,ESTABLISHED
    DOCKER     all  --  anywhere             anywhere            
    ACCEPT     all  --  anywhere             anywhere            
    ACCEPT     all  --  anywhere             anywhere            

    Chain OUTPUT (policy ACCEPT)
    target     prot opt source               destination         

    Chain DOCKER (3 references)
    target     prot opt source               destination         
    ACCEPT     tcp  --  anywhere             172.17.0.3           tcp dpt:cbt

    Chain DOCKER-ISOLATION-STAGE-1 (1 references)
    target     prot opt source               destination         
    DOCKER-ISOLATION-STAGE-2  all  --  anywhere             anywhere            
    DOCKER-ISOLATION-STAGE-2  all  --  anywhere             anywhere            
    DOCKER-ISOLATION-STAGE-2  all  --  anywhere             anywhere            
    RETURN     all  --  anywhere             anywhere            

    Chain DOCKER-USER (1 references)
    target     prot opt source               destination         
    RETURN     all  --  anywhere             anywhere            

    Chain DOCKER-ISOLATION-STAGE-2 (3 references)
    target     prot opt source               destination         
    DROP       all  --  anywhere             anywhere            
    DROP       all  --  anywhere             anywhere            
    DROP       all  --  anywhere             anywhere            
    RETURN     all  --  anywhere             anywhere 
    ```

# 2. Cgroups

## A. Découverte manuelle
- 🌞
```
[root@docker tp2]# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                    NAMES
3eb8af516d8c        debian              "sleep 99999"       17 minutes ago      Up 17 minutes       0.0.0.0:8888->7777/tcp   wizardly_nightingale
705fd1b52b6b        debian              "sleep 99999"       About an hour ago   Up About an hour                             naughty_goldstine
[root@docker tp2]# docker top 705fd1b52b6b
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                122215              122199              0                   20:49               ?                   00:00:00            sleep 99999
[root@docker tp2]# cat /proc/122215/cgroup
12:rdma:/
11:memory:/docker/705fd1b52b6b62ad073081278eb4c65fbae9e6225cdff703438145ec6b8c2ca5
10:hugetlb:/docker/705fd1b52b6b62ad073081278eb4c65fbae9e6225cdff703438145ec6b8c2ca5
9:cpu,cpuacct:/docker/705fd1b52b6b62ad073081278eb4c65fbae9e6225cdff703438145ec6b8c2ca5
8:devices:/docker/705fd1b52b6b62ad073081278eb4c65fbae9e6225cdff703438145ec6b8c2ca5
7:cpuset:/docker/705fd1b52b6b62ad073081278eb4c65fbae9e6225cdff703438145ec6b8c2ca5
6:perf_event:/docker/705fd1b52b6b62ad073081278eb4c65fbae9e6225cdff703438145ec6b8c2ca5
5:pids:/docker/705fd1b52b6b62ad073081278eb4c65fbae9e6225cdff703438145ec6b8c2ca5
4:net_cls,net_prio:/docker/705fd1b52b6b62ad073081278eb4c65fbae9e6225cdff703438145ec6b8c2ca5
3:blkio:/docker/705fd1b52b6b62ad073081278eb4c65fbae9e6225cdff703438145ec6b8c2ca5
2:freezer:/docker/705fd1b52b6b62ad073081278eb4c65fbae9e6225cdff703438145ec6b8c2ca5
1:name=systemd:/docker/705fd1b52b6b62ad073081278eb4c65fbae9e6225cdff703438145ec6b8c2ca5
```
## B. Utilisation par Docker
- 🌞
```
[root@docker tp2]# cat /sys/fs/cgroup/memory/docker/memory.max_usage_in_bytes 
80400384
[root@docker tp2]# cat /sys/fs/cgroup/pids/docker/pids.max 
max
[root@docker tp2]# cat /sys/fs/cgroup/cpuset/docker/cpuset.effective_cpus 
0-1
```
- 🌞
```
[root@docker tp2]# docker run -d --memory=4m --pids-limit=10 --device=/dev/sda1:/dev/root:r debian sleep 99999
063eaf23494e660cb00376f80bcf81e3f5e4e31bcf380038886d3e8080766317
[root@docker tp2]# docker top $(docker ps -lq)
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                124421              124404              0                   22:23               ?                   00:00:00            sleep 99999
[root@docker tp2]# cat /sys/fs/cgroup/memory/docker/memory.usage_in_bytes 
25620480
```

# 3. Capabilities


# A. Découverte manuelle

- 🌞
```
[root@docker tp2]# ps -e | grep bash
  8540 tty1     00:00:00 bash
123117 ?        00:00:00 bash
123938 pts/1    00:00:02 bash
125095 pts/2    00:00:00 bash
125486 pts/3    00:00:00 bash
[root@docker tp2]# cat /proc/8540/status | grep -i cap
CapInh: 0000000000000000
CapPrm: 0000003fffffffff
CapEff: 0000003fffffffff
CapBnd: 0000003fffffffff
CapAmb: 0000000000000000
[root@docker tp2]# capsh --decode=0000003fffffffff
0x0000003fffffffff=cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,cap_wake_alarm,cap_block_suspend,cap_audit_read
```
- 🌞
```
[root@docker tp2]# docker run -d alpine sleep 99999
4512d75449afea4edf9a7579dedb7f193a2576ad51871de1056ba0c0d7225f63
[root@docker tp2]# docker top $(docker ps -lq)
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                125968              125952              0                   23:05               ?                   00:00:00            sleep 99999
[root@docker tp2]# cat /proc/125968/status | grep -i cap
CapInh: 00000000a80425fb
CapPrm: 00000000a80425fb
CapEff: 00000000a80425fb
CapBnd: 00000000a80425fb
CapAmb: 0000000000000000
[root@docker tp2]# capsh --decode=00000000a80425fb
0x00000000a80425fb=cap_chown,cap_dac_override,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_net_bind_service,cap_net_raw,cap_sys_chroot,cap_mknod,cap_audit_write,cap_setfcap
```

- 🌞
    
    -
        ```
        [root@docker tp2]# find /*/*ping
        /bin/ping
        /sbin/arping
        /sbin/ping
        ```

    - 
        ```
        [root@docker tp2]# getcap /bin/ping
        /bin/ping = cap_net_admin,cap_net_raw+p
        ```
    - **❓❓❓❓** ya un probleme
        ```
        [root@docker tp2]# setcap '' /bin/ping
        [root@docker tp2]# ping 1.1.1.1 -c 1
        PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
        64 bytes from 1.1.1.1: icmp_seq=1 ttl=128 time=16.1 ms

        --- 1.1.1.1 ping statistics ---
        1 packets transmitted, 1 received, 0% packet loss, time 0ms
        rtt min/avg/max/mdev = 16.077/16.077/16.077/0.000 ms
        ```
        *n.b*: via un ``ping -c 40 1.1.1.1 >2 &`` je me retrouve avec sbin/ping mais getcap /sbin/ping donne rien car bin de root=dieu?

    -
        ```
        [root@docker tp2]# strace ping
        execve("/usr/sbin/ping", ["ping"], 0x7ffc20fbf780 /* 29 vars */) = 0
        brk(NULL)                               = 0x55fe4654b000
        arch_prctl(0x3001 /* ARCH_??? */, 0x7ffcae669120) = -1 EINVAL (Invalid argument)
        access("/etc/ld.so.preload", R_OK)      = -1 ENOENT (No such file or directory)
        openat(AT_FDCWD, "/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3
        fstat(3, {st_mode=S_IFREG|0644, st_size=39230, ...}) = 0
        mmap(NULL, 39230, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f195db27000
        close(3)                                = 0
        openat(AT_FDCWD, "/lib64/libcap.so.2", O_RDONLY|O_CLOEXEC) = 3
        read(3, "\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0\220\30\0\0\0\0\0\0"..., 832) = 832
        lseek(3, 15680, SEEK_SET)               = 15680
        read(3, "\4\0\0\0\20\0\0\0\5\0\0\0GNU\0\2\0\0\300\4\0\0\0\3\0\0\0\0\0\0\0", 32) = 32
        fstat(3, {st_mode=S_IFREG|0755, st_size=37952, ...}) = 0
        mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f195db25000
        lseek(3, 15680, SEEK_SET)               = 15680
        read(3, "\4\0\0\0\20\0\0\0\5\0\0\0GNU\0\2\0\0\300\4\0\0\0\3\0\0\0\0\0\0\0", 32) = 32
        mmap(NULL, 2117944, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f195d703000
        mprotect(0x7f195d707000, 2097152, PROT_NONE) = 0
        mmap(0x7f195d907000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x4000) = 0x7f195d907000
        close(3)                                = 0
        openat(AT_FDCWD, "/lib64/libidn2.so.0", O_RDONLY|O_CLOEXEC) = 3
        read(3, "\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0\300\31\0\0\0\0\0\0"..., 832) = 832
        lseek(3, 114072, SEEK_SET)              = 114072
        read(3, "\4\0\0\0\20\0\0\0\5\0\0\0GNU\0\2\0\0\300\4\0\0\0\3\0\0\0\0\0\0\0", 32) = 32
        fstat(3, {st_mode=S_IFREG|0755, st_size=165624, ...}) = 0
        lseek(3, 114072, SEEK_SET)              = 114072
        read(3, "\4\0\0\0\20\0\0\0\5\0\0\0GNU\0\2\0\0\300\4\0\0\0\3\0\0\0\0\0\0\0", 32) = 32
        mmap(NULL, 2215952, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f195d4e5000
        mprotect(0x7f195d501000, 2097152, PROT_NONE) = 0
        mmap(0x7f195d701000, 4096, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x1c000) = 0x7f195d701000
        mmap(0x7f195d702000, 16, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7f195d702000
        close(3)                                = 0
        openat(AT_FDCWD, "/lib64/libcrypto.so.1.1", O_RDONLY|O_CLOEXEC) = 3
        read(3, "\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0\0\240\7\0\0\0\0\0"..., 832) = 832
        fstat(3, {st_mode=S_IFREG|0755, st_size=4671512, ...}) = 0
        mmap(NULL, 5103496, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f195d007000
        mprotect(0x7f195d2b3000, 2093056, PROT_NONE) = 0
        mmap(0x7f195d4b2000, 192512, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x2ab000) = 0x7f195d4b2000
        mmap(0x7f195d4e1000, 16264, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7f195d4e1000
        close(3)                                = 0
        openat(AT_FDCWD, "/lib64/libresolv.so.2", O_RDONLY|O_CLOEXEC) = 3
        read(3, "\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0p<\0\0\0\0\0\0"..., 832) = 832
        fstat(3, {st_mode=S_IFREG|0755, st_size=114104, ...}) = 0
        mmap(NULL, 2189952, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f195cdf0000
        mprotect(0x7f195ce04000, 2093056, PROT_NONE) = 0
        mmap(0x7f195d003000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x13000) = 0x7f195d003000
        mmap(0x7f195d005000, 6784, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7f195d005000
        close(3)                                = 0
        openat(AT_FDCWD, "/lib64/libm.so.6", O_RDONLY|O_CLOEXEC) = 3
        read(3, "\177ELF\2\1\1\3\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0\20\305\0\0\0\0\0\0"..., 832) = 832
        fstat(3, {st_mode=S_IFREG|0755, st_size=2303696, ...}) = 0
        mmap(NULL, 3674432, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f195ca6e000
        mprotect(0x7f195cbee000, 2097152, PROT_NONE) = 0
        mmap(0x7f195cdee000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x180000) = 0x7f195cdee000
        close(3)                                = 0
        openat(AT_FDCWD, "/lib64/libc.so.6", O_RDONLY|O_CLOEXEC) = 3
        read(3, "\177ELF\2\1\1\3\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0\2009\2\0\0\0\0\0"..., 832) = 832
        fstat(3, {st_mode=S_IFREG|0755, st_size=3197880, ...}) = 0
        mmap(NULL, 3942432, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f195c6ab000
        mprotect(0x7f195c865000, 2093056, PROT_NONE) = 0
        mmap(0x7f195ca64000, 24576, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x1b9000) = 0x7f195ca64000
        mmap(0x7f195ca6a000, 14368, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7f195ca6a000
        close(3)                                = 0
        openat(AT_FDCWD, "/lib64/libunistring.so.2", O_RDONLY|O_CLOEXEC) = 3
        read(3, "\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0000'\1\0\0\0\0\0"..., 832) = 832
        lseek(3, 1558336, SEEK_SET)             = 1558336
        read(3, "\4\0\0\0\20\0\0\0\5\0\0\0GNU\0\2\0\0\300\4\0\0\0\3\0\0\0\0\0\0\0", 32) = 32
        fstat(3, {st_mode=S_IFREG|0755, st_size=1805368, ...}) = 0
        mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f195db23000
        lseek(3, 1558336, SEEK_SET)             = 1558336
        read(3, "\4\0\0\0\20\0\0\0\5\0\0\0GNU\0\2\0\0\300\4\0\0\0\3\0\0\0\0\0\0\0", 32) = 32
        mmap(NULL, 3672296, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f195c32a000
        mprotect(0x7f195c4a7000, 2093056, PROT_NONE) = 0
        mmap(0x7f195c6a6000, 20480, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x17c000) = 0x7f195c6a6000
        close(3)                                = 0
        openat(AT_FDCWD, "/lib64/libz.so.1", O_RDONLY|O_CLOEXEC) = 3
        read(3, "\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0\0'\0\0\0\0\0\0"..., 832) = 832
        lseek(3, 88296, SEEK_SET)               = 88296
        read(3, "\4\0\0\0\20\0\0\0\5\0\0\0GNU\0\2\0\0\300\4\0\0\0\3\0\0\0\0\0\0\0", 32) = 32
        fstat(3, {st_mode=S_IFREG|0755, st_size=101032, ...}) = 0
        lseek(3, 88296, SEEK_SET)               = 88296
        read(3, "\4\0\0\0\20\0\0\0\5\0\0\0GNU\0\2\0\0\300\4\0\0\0\3\0\0\0\0\0\0\0", 32) = 32
        mmap(NULL, 2187272, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f195c113000
        mprotect(0x7f195c129000, 2093056, PROT_NONE) = 0
        mmap(0x7f195c328000, 4096, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x15000) = 0x7f195c328000
        mmap(0x7f195c329000, 8, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7f195c329000
        close(3)                                = 0
        openat(AT_FDCWD, "/lib64/libdl.so.2", O_RDONLY|O_CLOEXEC) = 3
        read(3, "\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0\220\20\0\0\0\0\0\0"..., 832) = 832
        fstat(3, {st_mode=S_IFREG|0755, st_size=28784, ...}) = 0
        mmap(NULL, 2109744, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f195bf0f000
        mprotect(0x7f195bf12000, 2093056, PROT_NONE) = 0
        mmap(0x7f195c111000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x2000) = 0x7f195c111000
        close(3)                                = 0
        openat(AT_FDCWD, "/lib64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = 3
        read(3, "\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0000o\0\0\0\0\0\0"..., 832) = 832
        fstat(3, {st_mode=S_IFREG|0755, st_size=326696, ...}) = 0
        mmap(NULL, 2225344, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f195bcef000
        mprotect(0x7f195bd0a000, 2093056, PROT_NONE) = 0
        mmap(0x7f195bf09000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x1a000) = 0x7f195bf09000
        mmap(0x7f195bf0b000, 13504, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7f195bf0b000
        close(3)                                = 0
        mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f195db21000
        arch_prctl(ARCH_SET_FS, 0x7f195db22480) = 0
        mprotect(0x7f195ca64000, 16384, PROT_READ) = 0
        mprotect(0x7f195bf09000, 4096, PROT_READ) = 0
        mprotect(0x7f195c111000, 4096, PROT_READ) = 0
        mprotect(0x7f195c328000, 4096, PROT_READ) = 0
        mprotect(0x7f195c6a6000, 16384, PROT_READ) = 0
        mprotect(0x7f195cdee000, 4096, PROT_READ) = 0
        mprotect(0x7f195d003000, 4096, PROT_READ) = 0
        mprotect(0x7f195d4b2000, 176128, PROT_READ) = 0
        mprotect(0x7f195d701000, 4096, PROT_READ) = 0
        mprotect(0x7f195d907000, 4096, PROT_READ) = 0
        mprotect(0x55fe45f92000, 4096, PROT_READ) = 0
        mprotect(0x7f195db31000, 4096, PROT_READ) = 0
        munmap(0x7f195db27000, 39230)           = 0
        set_tid_address(0x7f195db22750)         = 126269
        set_robust_list(0x7f195db22760, 24)     = 0
        rt_sigaction(SIGRTMIN, {sa_handler=0x7f195bcf59a0, sa_mask=[], sa_flags=SA_RESTORER|SA_SIGINFO, sa_restorer=0x7f195bd01dd0}, NULL, 8) = 0
        rt_sigaction(SIGRT_1, {sa_handler=0x7f195bcf5a30, sa_mask=[], sa_flags=SA_RESTORER|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f195bd01dd0}, NULL, 8) = 0
        rt_sigprocmask(SIG_UNBLOCK, [RTMIN RT_1], NULL, 8) = 0
        prlimit64(0, RLIMIT_STACK, NULL, {rlim_cur=8192*1024, rlim_max=RLIM64_INFINITY}) = 0
        access("/etc/system-fips", F_OK)        = -1 ENOENT (No such file or directory)
        brk(NULL)                               = 0x55fe4654b000
        brk(0x55fe4656c000)                     = 0x55fe4656c000
        brk(NULL)                               = 0x55fe4656c000
        capget({version=_LINUX_CAPABILITY_VERSION_3, pid=0}, NULL) = 0
        capget({version=_LINUX_CAPABILITY_VERSION_3, pid=0}, {effective=1<<CAP_CHOWN|1<<CAP_DAC_OVERRIDE|1<<CAP_DAC_READ_SEARCH|1<<CAP_FOWNER|1<<CAP_FSETID|1<<CAP_KILL|1<<CAP_SETGID|1<<CAP_SETUID|1<<CAP_SETPCAP|1<<CAP_LINUX_IMMUTABLE|1<<CAP_NET_BIND_SERVICE|1<<CAP_NET_BROADCAST|1<<CAP_NET_ADMIN|1<<CAP_NET_RAW|1<<CAP_IPC_LOCK|1<<CAP_IPC_OWNER|1<<CAP_SYS_MODULE|1<<CAP_SYS_RAWIO|1<<CAP_SYS_CHROOT|1<<CAP_SYS_PTRACE|1<<CAP_SYS_PACCT|1<<CAP_SYS_ADMIN|1<<CAP_SYS_BOOT|1<<CAP_SYS_NICE|1<<CAP_SYS_RESOURCE|1<<CAP_SYS_TIME|1<<CAP_SYS_TTY_CONFIG|1<<CAP_MKNOD|1<<CAP_LEASE|1<<CAP_AUDIT_WRITE|1<<CAP_AUDIT_CONTROL|1<<CAP_SETFCAP|1<<CAP_MAC_OVERRIDE|1<<CAP_MAC_ADMIN|1<<CAP_SYSLOG|1<<CAP_WAKE_ALARM|1<<CAP_BLOCK_SUSPEND|1<<CAP_AUDIT_READ, permitted=1<<CAP_CHOWN|1<<CAP_DAC_OVERRIDE|1<<CAP_DAC_READ_SEARCH|1<<CAP_FOWNER|1<<CAP_FSETID|1<<CAP_KILL|1<<CAP_SETGID|1<<CAP_SETUID|1<<CAP_SETPCAP|1<<CAP_LINUX_IMMUTABLE|1<<CAP_NET_BIND_SERVICE|1<<CAP_NET_BROADCAST|1<<CAP_NET_ADMIN|1<<CAP_NET_RAW|1<<CAP_IPC_LOCK|1<<CAP_IPC_OWNER|1<<CAP_SYS_MODULE|1<<CAP_SYS_RAWIO|1<<CAP_SYS_CHROOT|1<<CAP_SYS_PTRACE|1<<CAP_SYS_PACCT|1<<CAP_SYS_ADMIN|1<<CAP_SYS_BOOT|1<<CAP_SYS_NICE|1<<CAP_SYS_RESOURCE|1<<CAP_SYS_TIME|1<<CAP_SYS_TTY_CONFIG|1<<CAP_MKNOD|1<<CAP_LEASE|1<<CAP_AUDIT_WRITE|1<<CAP_AUDIT_CONTROL|1<<CAP_SETFCAP|1<<CAP_MAC_OVERRIDE|1<<CAP_MAC_ADMIN|1<<CAP_SYSLOG|1<<CAP_WAKE_ALARM|1<<CAP_BLOCK_SUSPEND|1<<CAP_AUDIT_READ, inheritable=0}) = 0
        capget({version=_LINUX_CAPABILITY_VERSION_3, pid=0}, NULL) = 0
        capset({version=_LINUX_CAPABILITY_VERSION_3, pid=0}, {effective=0, permitted=1<<CAP_NET_ADMIN|1<<CAP_NET_RAW, inheritable=0}) = 0
        prctl(PR_SET_KEEPCAPS, 1)               = 0
        getuid()                                = 0
        setuid(0)                               = 0
        prctl(PR_SET_KEEPCAPS, 0)               = 0
        getuid()                                = 0
        geteuid()                               = 0
        openat(AT_FDCWD, "/usr/lib/locale/locale-archive", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
        openat(AT_FDCWD, "/usr/share/locale/locale.alias", O_RDONLY|O_CLOEXEC) = 3
        fstat(3, {st_mode=S_IFREG|0644, st_size=2997, ...}) = 0
        read(3, "# Locale name alias data base.\n#"..., 4096) = 2997
        read(3, "", 4096)                       = 0
        close(3)                                = 0
        openat(AT_FDCWD, "/usr/lib/locale/en_US.UTF-8/LC_IDENTIFICATION", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
        openat(AT_FDCWD, "/usr/lib/locale/en_US.utf8/LC_IDENTIFICATION", O_RDONLY|O_CLOEXEC) = 3
        fstat(3, {st_mode=S_IFREG|0644, st_size=368, ...}) = 0
        mmap(NULL, 368, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f195db30000
        close(3)                                = 0
        openat(AT_FDCWD, "/usr/lib64/gconv/gconv-modules.cache", O_RDONLY) = 3
        fstat(3, {st_mode=S_IFREG|0644, st_size=26998, ...}) = 0
        mmap(NULL, 26998, PROT_READ, MAP_SHARED, 3, 0) = 0x7f195db29000
        close(3)                                = 0
        futex(0x7f195ca69a28, FUTEX_WAKE_PRIVATE, 2147483647) = 0
        openat(AT_FDCWD, "/usr/lib/locale/en_US.UTF-8/LC_MEASUREMENT", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
        openat(AT_FDCWD, "/usr/lib/locale/en_US.utf8/LC_MEASUREMENT", O_RDONLY|O_CLOEXEC) = 3
        fstat(3, {st_mode=S_IFREG|0644, st_size=23, ...}) = 0
        mmap(NULL, 23, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f195db28000
        close(3)                                = 0
        openat(AT_FDCWD, "/usr/lib/locale/en_US.UTF-8/LC_TELEPHONE", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
        openat(AT_FDCWD, "/usr/lib/locale/en_US.utf8/LC_TELEPHONE", O_RDONLY|O_CLOEXEC) = 3
        fstat(3, {st_mode=S_IFREG|0644, st_size=59, ...}) = 0
        mmap(NULL, 59, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f195db27000
        close(3)                                = 0
        openat(AT_FDCWD, "/usr/lib/locale/en_US.UTF-8/LC_ADDRESS", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
        openat(AT_FDCWD, "/usr/lib/locale/en_US.utf8/LC_ADDRESS", O_RDONLY|O_CLOEXEC) = 3
        fstat(3, {st_mode=S_IFREG|0644, st_size=167, ...}) = 0
        mmap(NULL, 167, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f195db20000
        close(3)                                = 0
        openat(AT_FDCWD, "/usr/lib/locale/en_US.UTF-8/LC_NAME", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
        openat(AT_FDCWD, "/usr/lib/locale/en_US.utf8/LC_NAME", O_RDONLY|O_CLOEXEC) = 3
        fstat(3, {st_mode=S_IFREG|0644, st_size=77, ...}) = 0
        mmap(NULL, 77, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f195db1f000
        close(3)                                = 0
        openat(AT_FDCWD, "/usr/lib/locale/en_US.UTF-8/LC_PAPER", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
        openat(AT_FDCWD, "/usr/lib/locale/en_US.utf8/LC_PAPER", O_RDONLY|O_CLOEXEC) = 3
        fstat(3, {st_mode=S_IFREG|0644, st_size=34, ...}) = 0
        mmap(NULL, 34, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f195db1e000
        close(3)                                = 0
        openat(AT_FDCWD, "/usr/lib/locale/en_US.UTF-8/LC_MESSAGES", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
        openat(AT_FDCWD, "/usr/lib/locale/en_US.utf8/LC_MESSAGES", O_RDONLY|O_CLOEXEC) = 3
        fstat(3, {st_mode=S_IFDIR|0755, st_size=29, ...}) = 0
        close(3)                                = 0
        openat(AT_FDCWD, "/usr/lib/locale/en_US.utf8/LC_MESSAGES/SYS_LC_MESSAGES", O_RDONLY|O_CLOEXEC) = 3
        fstat(3, {st_mode=S_IFREG|0644, st_size=57, ...}) = 0
        mmap(NULL, 57, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f195db1d000
        close(3)                                = 0
        openat(AT_FDCWD, "/usr/lib/locale/en_US.UTF-8/LC_MONETARY", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
        openat(AT_FDCWD, "/usr/lib/locale/en_US.utf8/LC_MONETARY", O_RDONLY|O_CLOEXEC) = 3
        fstat(3, {st_mode=S_IFREG|0644, st_size=286, ...}) = 0
        mmap(NULL, 286, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f195db1c000
        close(3)                                = 0
        openat(AT_FDCWD, "/usr/lib/locale/en_US.UTF-8/LC_COLLATE", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
        openat(AT_FDCWD, "/usr/lib/locale/en_US.utf8/LC_COLLATE", O_RDONLY|O_CLOEXEC) = 3
        fstat(3, {st_mode=S_IFREG|0644, st_size=2586930, ...}) = 0
        mmap(NULL, 2586930, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f195ba77000
        close(3)                                = 0
        openat(AT_FDCWD, "/usr/lib/locale/en_US.UTF-8/LC_TIME", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
        openat(AT_FDCWD, "/usr/lib/locale/en_US.utf8/LC_TIME", O_RDONLY|O_CLOEXEC) = 3
        fstat(3, {st_mode=S_IFREG|0644, st_size=3316, ...}) = 0
        mmap(NULL, 3316, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f195db1b000
        close(3)                                = 0
        openat(AT_FDCWD, "/usr/lib/locale/en_US.UTF-8/LC_NUMERIC", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
        openat(AT_FDCWD, "/usr/lib/locale/en_US.utf8/LC_NUMERIC", O_RDONLY|O_CLOEXEC) = 3
        fstat(3, {st_mode=S_IFREG|0644, st_size=54, ...}) = 0
        mmap(NULL, 54, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f195db1a000
        close(3)                                = 0
        openat(AT_FDCWD, "/usr/lib/locale/en_US.UTF-8/LC_CTYPE", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
        openat(AT_FDCWD, "/usr/lib/locale/en_US.utf8/LC_CTYPE", O_RDONLY|O_CLOEXEC) = 3
        fstat(3, {st_mode=S_IFREG|0644, st_size=337024, ...}) = 0
        mmap(NULL, 337024, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f195dac7000
        close(3)                                = 0
        write(2, "Usage: ping [-aAbBdDfhLnOqrRUvV6"..., 286Usage: ping [-aAbBdDfhLnOqrRUvV64] [-c count] [-i interval] [-I interface]
                    [-m mark] [-M pmtudisc_option] [-l preload] [-p pattern] [-Q tos]
                    [-s packetsize] [-S sndbuf] [-t ttl] [-T timestamp_option]
                    [-w deadline] [-W timeout] [hop1 ...] destination
        ) = 286
        write(2, "Usage: ping -6 [-aAbBdDfhLnOqrRU"..., 316Usage: ping -6 [-aAbBdDfhLnOqrRUvV] [-c count] [-i interval] [-I interface]
                    [-l preload] [-m mark] [-M pmtudisc_option]
                    [-N nodeinfo_option] [-p pattern] [-Q tclass] [-s packetsize]
                    [-S sndbuf] [-t ttl] [-T timestamp_option] [-w deadline]
                    [-W timeout] destination
        ) = 316
        exit_group(2)                           = ?
        +++ exited with 2 +++
        [root@docker tp2]# 
        ```

# B. Utilisation par Docker

- 🌞
setuid= pour avoir des utilisateurs
setgid= pour avoir les groupes
net_bind_service=avoir access au réseau

```
[root@docker tp2]# docker run -p 8080:80 --cap-drop=all --cap-add=chown --cap-add=setgid --cap-add=setuid --cap-add=net_bind_service -d nginx:alpine
17295100378a84d3e0578c38e153086e0987c2426bbb5fe66ff87af8d062fcc1
[root@docker tp2]# curl localhost:8080
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```