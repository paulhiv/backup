# TP1: Prise en main de Docker

# Docker

* lancement de notre premier container
```
[root@docker tp1]# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED              STATUS              PORTS               NAMES
59cb86dcee63        alpine              "sleep 9999"        About a minute ago   Up About a minute                       jovial_meitner
[root@docker tp1]# docker exec -it 59cb86dcee63 sh
/ # echo "nous sommes dans le container alpine"
nous sommes dans le container alpine
/ # exit
```
grâce a la commande docker inspect nous pouvons voir que le containeur est opére de façon totalement idépendante ou sinon on peut vérifier en dur
```
[paulh@docker tp1]$ docker exec -it a2210c84943e19f6678e5223ed0296ecf6ee22b42b57fc554dd6232e4353bec5 /bin/sh
/ # whoami
root
/ # ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
17: eth0@if18: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
/ # fdisk -l
/ # 
[paulh@docker tp1]# docker stop 59cb86dcee63
59cb86dcee63
[paulh@docker tp1]# docker rm 59cb86dcee63
59cb86dcee63
```

## lancement d'un containeur nginx
```
[root@docker tp1]# docker run -p 80:80 -d nginx
5538032b2492ba6e87db9e00bc6a7a03509856b61c43c2213a3ee15aaa06def7
[root@docker tp1]# curl localhost:80
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```
## Gestion d'image
```
[root@docker tp1]# docker pull httpd:2.2
2.2: Pulling from library/httpd
f49cf87b52c1: Pull complete 
24b1e09cbcb7: Pull complete 
8a4e0d64e915: Pull complete 
bcbe0eb4ca51: Pull complete 
16e370c15d38: Pull complete 
Digest: sha256:9784d70c8ea466fabd52b0bc8cde84980324f9612380d22fbad2151df9a430eb
Status: Downloaded newer image for httpd:2.2
docker.io/library/httpd:2.2
[root@docker tp1]# docker run -p 80:80 -d httpd:2.2
[root@docker tp1]# curl localhost:80
<html><body><h1>It works!</h1></body></html>[root@docker tp1]# 
```
## créer une image qui lance un serveur web python
```
root@docker tp1]# docker build -t python_server . 
Sending build context to Docker daemon   34.3kB
Step 1/6 : FROM python:alpine
 ---> 4c30403e92a1
Step 2/6 : EXPOSE 8888
 ---> Using cache
 ---> 78c6b1f6c2bb
Step 3/6 : RUN python3
 ---> Running in fad794885e61
Removing intermediate container fad794885e61
 ---> 4eef333373e9
Step 4/6 : WORKDIR /srv
 ---> Running in ccf0e22c051a
Removing intermediate container ccf0e22c051a
 ---> 3dd08c574f4c
Step 5/6 : COPY ./python-app /srv
 ---> 5d7a1115c291
Step 6/6 : CMD python3 -m http.server 8888
 ---> Running in 3f728875b479
Removing intermediate container 3f728875b479
 ---> c344ef04127e
Successfully built c344ef04127e
Successfully tagged python_server:latest
```
* ensuite le lancer avec
```
[root@docker tp1]# docker run -p 7777:8888 -d python_server
5dcc1766b89f61475c812f31f8cddd455b62c1f529cd8831e3340b595ed04dcf
[root@docker tp1]# curl localhost:7777
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="app.py">app.py</a></li>
<li><a href="requirements">requirements</a></li>
<li><a href="templates/">templates/</a></li>
</ul>
<hr>
</body>
</html>
[root@docker tp1]# 
```

## Modifier la configuration du démon Docker:

- le path du socket pour le daemon docker se trouve sur ``/var/run/docker.sock``

- on peut faire écouter le daemon avec la commande
  ``dockerd -H tcp://0.0.0.0:2376``

# Docker compose:

##  Ecrire un docker-compose-v1.yml:
```
version: "3.3"

services:
  server1:
    image: python_server
    networks:
      compose-net:
        aliases:
          - docker_server.test
          - docker_server
    ports:
      - "8080:8888"

networks:
  compose-net:
```
avec
```
[root@docker tp1]# curl localhost:8080
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="app.py">app.py</a></li>
<li><a href="requirements">requirements</a></li>
<li><a href="templates/">templates/</a></li>
</ul>
<hr>
</body>
</html>
[root@docker tp1]# curl localhost:7777
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="app.py">app.py</a></li>
<li><a href="requirements">requirements</a></li>
<li><a href="templates/">templates/</a></li>
</ul>
<hr>
</body>
</html>
[root@docker tp1]# 
```

## Ajouter un deuxième conteneur `docker-compose-v2.yml`

création du certificat ssl
```
[root@docker tp]# openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./tp1/certs/test.docker.key -out ./tp1/certs/test.docker.crt
Generating a RSA private key
...........................+++++
..................+++++
writing new private key to './tp1/certs/test.docker.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:fr
State or Province Name (full name) []:aq
Locality Name (eg, city) [Default City]:bdx
Organization Name (eg, company) [Default Company Ltd]:ynov
Organizational Unit Name (eg, section) []:ing 
Common Name (eg, your name or your server's hostname) []:test.docker
Email Address []:paul.hivert@ynov.Com
```
dockerfile utilisé pour python_server

```
FROM python:alpine
EXPOSE 8888
RUN python3
WORKDIR /python-app
COPY ./python-app /pyhton-app
CMD python3 -m http.server 8888
```

dockerfile utilisé pour nginx:mine
```
FROM nginx:1.10.2-alpine
EXPOSE 8888
WORKDIR /certs
COPY ./certs /certs
COPY ./nginx/test.docker.conf /etc/nginx/conf.d/test.docker.conf
CMD nginx
```
docker-compose-v2
```
version: "3.3"

services:
  server1:
    image: python_server
    networks:
      compose-net:
        aliases:
          - docker_server.test
          - docker_server
    ports:
      - "8080:8888"
  server2:
    image: nginx
    build: .
    networks:
      compose-net:
        aliases:
          - docker_server.test
          - docker_server
    ports:
      - "7777:8888"

networks:
  compose-net:
```
