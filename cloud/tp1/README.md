# dérnier soleil
## le rendu entier du tp se trouve [ici](./tp1.md)

pour lancer le service utilisez la commande ``docker-compose up -d --build``

se docker compose
contient:
- un server python
    - avec comme alias python-app
    - appartenant au réseau compose-net
    - utilisant l'image officielle de python alpine - construit avec le fichier [python-dockerfile](./pythondockerfile)
    - avec un volume
    - avec le port 443 joignable
- un serveur nginx
    - avec comme alias nginx
    - appartenant au réseau compose-net
    - utilisant l'image officelle de nginx alpine
    - construit avec le fichier [nginx-dockerfile](./nginx-dockerfile)
    - avec le port 443 joignable
- un serveur redis
    - avec comme alias db
    - apartenant au réseau compose-net
    - utilisant l'image officielle de redis alpine
    avec le port 6379 joignable
