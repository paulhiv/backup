# mise en place du lab

- configuration des hosts
    ``sudo hostnamectl set-hostname node\d\.b3``

- déactivation de SELinux
    ``sudo setenfoce 0``

- décativation du firewall
    ``sudo systemctl disable firewalld && systemctl stop firewalld``

- mise en place des hosts
    ``echo "<ip> node\d node\d.b3 \n <ip> node\d node\d.b3" | tee -a /etc/hosts``

- installation docker
```
sudo yum install -y yum-utils device-mapper-persistent-data lvm2 && sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo && sudo yum-config-manager --enable docker-ce-nightly && sudo yum install docker-ce docker-ce-cli containerd.io -y
```

- installation docker-compose
```
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && sudo chmod +x /usr/local/bin/docker-compose && sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

- 