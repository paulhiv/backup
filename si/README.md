# methode ITIL v3-v4
## c'est quoi ITIL
- definition: inforamation technoligoies infrastructure library
- ITIL: seule démarche pragmatique reposant sur un ensemble des meuilleurs pratiques issues d'experience
## utilisation ITIL
1. ITIL est d'aptoder une démarche basé sur des processus scallable
    - example:
        -  liste de best pratices
2. obejctif gestion de services et ressources humaines.
    - examples: 
        - rédaction de doc systematique
        - [diagramme de pareto](https://fr.wikipedia.org/wiki/Diagramme_de_Pareto)
3. les organisations ne doivent pas être trop ambitieuses lors de la mise en oeuvre de la gestion des services **centré sur l'améloiration des processus existants**
    - examples:
        - [roue de deming](https://www.piloter.org/qualite/roue-de-deming-PDCA.htm), [loi de murphy](https://en.wikipedia.org/wiki/Murphy%27s_law), 
        - methode des 6P 
            1. technique
            2. organisationel: RTO, RPO, SLA, GTI, GTR...
            3. humain: combien d'heures pour faire le projet
            4. financier: budget
            5. juridique
            6. green IT: dévolppemment durable
4. Pour ce faire, il est nécessaire de bien connaitre son point de départ
en évaluant la maturité de ses processus existants
    - exemple: 
        - s'assurer de l'implication du management pour engager une
        telle démarche et que les conditions d'un changement culturel sont satisfaites pour modifier le comportement de l'organisation dans la fourniture des services
5. Les processus de gestion des services peuvent être mis en ceuvre les
uns à la suite des autres ou simultanement et chaque processus peut
être decomposé en une serie d'activités. L'utilisation de ces meilleures
pratiques est soutenue par un éventail de formations et de
certifications qui est utilisé dans le monde entier pour reconnaitre les
compétences professionnelles nécessaires en matière de Gestion des
Services liés aux technologies de l'information.  
    - example:
        [méthode RACI](https://www.manager-go.com/gestion-de-projet/dossiers-methodes/matrice-raci)
## objectifs
- Aligner les services lies aux technologies de l'information avec les
besoins présents et futurs des métiers de l'entreprise et de ses clients
- Amélorer la qualité des services liés aux technologies de l'information
- Réduire à long terme les couts liés aux préstations de services
## principes d'ITIL
- le client: les demandes des clients sont primordiales
- cycle de vie des projets 
    - assurer la fiabilité des projets dés la conception
    - PCDA
    TCO: Total cost of ownership aka ammortisement avec côut d'opportunité avec un aspect interopératibilité
- maitriser les processus la qualité de service se fonde sur approche par les processus
    - régit sur la définition de livrables versionés et de doctrines
## domaines couverts par ITIL
- ITIL définitun service lié aux technologies de l'inforamtion comme un ensemble de fonctions assurées par un système d'information pour répondre aux besoins d'un utilisateur dans la réalisation de ses activités ppropre à son métier; un service s'appuie en général sur plsieurs élément.
(charte de nommage penser de mettre 8 digits)
## trois versions d'ITIL
- la version 1
    - était lancée par le gouvernement britanique fin des années 1980
    - la production des 40 livres
    - ITIL de cette version a duré jusqu'au début des années 1990
- la version 2
    - était élabrée entre le mileu des ann"es 1990 et 2004
    - produit 9 livres dont seulement 2 ont forgé la réputation de ITIL
    - Ces deux livres sont les plus connus et utilusés aujourd'hui
## pourquoi ITIL a-t-il tant de success
- service delivery
    - concerne la planification l'amélioration a long terme la fourniture de services liés aux technologies de l'information
        - gestion des nuveaux de service (service level management ou SLM)
        - la gestion financière des servuces des TI (IT service Financial management)
        - la gestion de la capacité (capacity management)
        - la gestion de la disponibitité ( availability mangement)
        - la gesion de la continuité des services de TI (IT service continuity management)
        - la gestion de la sécurité (security management)
        - la gestion de a sécurité (security management)
- service support
    - se concentre globalement sur les opérations au jour le jour et le support aux services liés aux téchnologies de l'information
        - le centre de services (service desk) est une fonction de l'organistion des TI
            - détection des incidents, prise des appels , coordination des actions
        - la gestion des incidents (incident management)
            - enregistremnet des incidents, processis de gestion des incidents, classification escalade et remontée
        - la gestion de problèmes (problem management)
            - traitenment des erreurs connue, interralation entre les processus
        - la gestion des changements (change management)
            - péparer et valider un chagement, traitement des demandes de changement
        - la gestion des mises en production (release management)
            - modélisation des l'infrastructire IT, maitriser la gestion des éléments de confiiguration, base de données de gestion de la configuration
                (PRA PCA)
## ITIL v4 c'est quoi



## inconvéniants ITIL
## conclusion