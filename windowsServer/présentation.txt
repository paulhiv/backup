Pour le système:
MCP
MCSA
MCSE

Pour le réseaux:
CCNA
CCNP
CCIE

On va faire:
70-410
70-740

on va dans la partie "MCSA" -->
microsoft.com/fr-fr/learning.exam-70-410.aspx

les cours:
mva.microsoft.com
blog stanislas quastana
.
.

fichier pagefile.sys = swap


Le master boot record comporte 4 fichiers:
•    lo.sys : Contient les pilotes + lis le BIOS
•    autoexec.bat
Ce fichier charge des commandes selon l’ordre dans lequel elles sont écrites.
•    config.sys
Config.sys est chargé en premier par le DOS (Disk Operating System) et prend en charge la configuration des composants du PC.
•    ntldr
NTLDR permet de choisir, lorsque plusieurs systèmes Microsoft Windows sont installés sur une même machine, quel système on souhaite amorcer.
Le fichier de configuration de NTLDR, boot.ini est en ASCII.


S1AD1<-|->S2AD2 | CH
AD     | AD2    |
DNS    | DNS2   |
DHCP   |        |

S1AD1 = Server 1 Active Directory 1
S2AD2 = Server 2 Active Directory 2
CH = Client
HA = Hight Avaibility


10.33.10.1 /24
255.255.255.0
2^8-2



Admin = sur tous les PC = /Passw0rd


PAR VM:
1.5G RAM (1536Mo)
HDD1: 30Go
HDD2: 5Go
ETH0 : 1 carte réseaux (réseaux interne)
ETH1 : 1 carte réseaux (NAT ou Bridge)


CREATE NEW VIRTUAL MACHINE:
B2A-2018 S1AD1
changer user : Système ->Modifier les paramètres -> onglet Nom de l'ordinateur -> modifier -> redémarrer
Gestionnaire de serveur -> Serveur Local -> COnfiguration de sécurité renforcée Microsoft (a désactivé sur admin + user)

Panneau de Configuration -> Connexion réseaux ->
changer Ethernet 0 -> interne
changer Ethernet 1 -> externe


Rajouter "BGINFO" logiciel
(sers à afficher les info, type conky sur le bureau Windows)


Type de disque:
facteur important de stockage:  Taille --> Blocs (cluster) --> Vitesse --> tours/min
Type de disque: |     SATA         |       SAS       |        SSD       |  Mvme (M2)  |         SASSD         | SSHD
--------------------------------------------------------------------------------------------------------------|
Capacité :      |  500Go - 22To    |  300Go - 900Go  |   120Go - 4To    | 120Go - 2To |      74Go et 120Go    |
                |                  |    max 12To     |                  |             |                       |
--------------------------------------------------------------------------------------------------------------|
Vitesse t/min:  |   5400 - 7200    |   10 - 15 000   |         X        |      X      |           X           |
                |                  | (12To) --> 7200 |                  |             |                       |
--------------------------------------------------------------------------------------------------------------|
IOPS:           |    800 - 2000    |   2000 - 3000   | 15 000 - 110 000 |   450 000   | 1 200 000 - 3 000 000 |
(input/output   |                  |                 |                  |             |                       |
operation par s)|                  |                 |                  |             |                       |
--------------------------------------------------------------------------------------------------------------|
Prix:           |    40€ - 450€    |    280 - 450    |     50 - 500     |      800    |         1300€         |
                |                  |       700       |                  |             |                       |
--------------------------------------------------------------------------------------------------------------|

Méthode de stockage Tiering:
H (Hot) | M (Medium) | C (Cold) |
--------|------------|----------|
SSD     |     SAS    |   SATA   |
M2      |            |          |
NVME    |            |          |


LACP : permet de faire de l'agregat
https://fr.wikipedia.org/wiki/Agr%C3%A9gation_de_liens
.





Retour sur WIndows Server 2016, désactivation du pare feu:
Centre Réseaux et partage --> en bas à gauche "Pare Feu" --> "Modifier les paramètres de notification" --> tout désactiver

On édit la carte réseaux "interne", et on désactive l'IPv6
on crée une adresse IP en:
10.33.10.1 --> 255.255.255.0
Désactiver carte externe


Gestion des disques, redimentionner taille, crée disque L


Activer le partage d'un répertoire dans le lecteur L
créer dossier  "DATA01"
(Clic droit DATA01 --> partage --> partager --> ajouter "Tous le monde")

Chemin UNC:
\\nompartage\ (ici nompartage = S1AD1)


Ensuite Gestionnaire de serveur --> Outil --> Gestion de l'ordinateur puis:
Utilisateur et groupe --> utilisateur --> Nouvel utilisateur
user01 - user01 - password - cocher la 2eme & 3eme case - crée
aller dans "Stratégie de sécurité locale" --> "Stratégie de compte" --> stratégie de mot de passe --> Durée de vie max password, changer à 0




ensuite recrée/cloné 2eme VM:
Carte interne: 10.33.10.2

PAS OUBLIER DESACTIVER FIREWALL SUR TOUS LES SERVEURS


### AJOUTER DES IMPRESSIONS:


          |         NB        |          C        |
Jet Encre |   0.50€ -> 1.5€   |     0.7€ -> 2€    |
Laser     |        //         |         //        |
Copieur   | 0.0035€ -> 0.005€ |
          |3.5€ -> 5€ / 1000p | 35€ - 50€ / 1000p |
          |  300x moins cher  |                   |



Paramètre --> Imprimante et Scanner --> Ajouter des imprimantes et des scanners
Ajouter une imprimante locale ou Réseaux
Choisir HP 1600 Class Driver

Emplacement : Ynov G2
Commentaire: A côté de la machine à café
Ne pas imprimer de page de Test


Gerer imprimante --> Préférence d'impression --> Choisir A4 non couleur
                 --> Proprieter d'imprimante --> mettre a 100dpi ou +
                                             --> activer partage
                                             --> Avancer --> Priorité (1 forte priorité - 100 faible priorité)
                                                         --> Cocher "Conserver les documents imprimés"

aller sur S2AD2 et installer l'imprimante avec S2AD2

Sur S1AD1:
Gestionnaire de serveur --> Ajouter des rôles et des fonctionnalités
Suivant->Suivant->Suivant->(Rôle de serveur) cocher "Service d'impression et de numérisation" (click Ajouter des fonctionnalités) -> Suivant -->(Services de rôle) cocher "Serveur d'impression" et "Impression Internet" --> Suivant --> Suivant --> Suivant --> Installer --> Fermer

Gestionnaire de serveur --> Outil --> Gestion de l'impression
Gestion de l'impression --> Filtre personnalisés --> Toutes les imprimantes





Sur S2AD2
Gestionnaire de serveur --> onglet gauche Serveur local --> Configuration de sécurité renforcé d'Internet Explorer --> Inactif
Démarrer Internet Explorer --> http://S1AD1/


#### ANNUAIRE
Qu'est ce qu'un annuaire:
c'est une base de donnée qui fonctionne en lecture/écriture
il permet de définir des champs et des attribut d'un device

X500 c'est le premier à avoir existé
LDAP pour Linux s'en est suivi
maintenant Active Directory



### AJOUTER ACTIVE DIRECTORY:

Gestionnaire de serveur --> Ajouter des rôles et des fonctionnalités
Suivant->Suivant->Suivant->(Rôle de serveur) cocher "Service AD DS" (click Ajouter des fonctionnalités) -> Suivant --> Suivant --> Suivant --> Installer --> Fermer



REDEMARRER
Gestionnaire de serveur --> "notification" (avec "/!\") --> Promouvoir ce serveur en contrôleur de domaine
Ajouter nouvelle Forêt -->
DOMAIN NAME: 8 caractere max, pas de caractère accentué, pas d'espace
actuellement: DOMROUG.COM

Mettre 2012 sur les 2 niveaux fonctionnel

rien à coché/décoché
RODC = Read Only Domain Contrôler (permet de prendre la main sur un serveur enfant, utilisé pour les franchises/succursale)
GC = Global Catalog


password: /Passw0rd
Suivant --> domaine NetBIOS : DOMROUG , Suivant --> on laisse par défaut, Suivant --> "Afficher le script" montre comment on le fais en powershell , Suivant -->
(si erreur de DNS, ajouter un DNS 10.33.10.1) --> Installer
Attendre le Redémarrage


pour y acceder:
Gestionnaire de serveur --> Outils --> Utilisateur et ordinateur active Directory --> DOMROUG

### configurer le DNS:
Gestionnaire de serveur --> Outils --> DNS
Gestionnaire DNS --> DNS --> S1AD1 --> clic droit Zones de recherche inversé --> Nouvelle Zone
Suivant Suivant Suivant (ID Réseaux : 10.33.10) Suivant
                                   --> Zones de recherche directe --> DOMROUG.COM --> S1AD1 (clic droit propriété, cocher mettre à jours le pointeur PTR)
                    (clic droit) DNS --> Mettre à jours les fichiers de données du Serveur
Il existe les hôtes:
A
AAAA
PTR
SOA
MX

Redésactiver le pare feu de S1AD1

Aller sur S2AD2:
ajouter un DNS sur l'IP (10.33.10.1)
Ce PC -> clic droit propriété --> "Modifier les paramètres" --> se connecter au domaine S1AD1


//UTILE
djoin --



se connecter sur le domaine:
DOMAINE\Administrateur


pour ajouter un contrôleur de domaine secondaire:
Sur S2AD2: Gestionnaire de serveur --> "Ajouter Rôle et fonctionnalités" --> Cocher: Service AD DS
Assistant configuration Active Directory --> Vérifier de bien avoir "DOMAINE\Administrateur" --> Suivant
Cocher "Serveur DNS", "Catalogue Global", mettre le mot de passe --> Suivant Suivant Suivant Installer


le IFM = Install From Media sers à installer sur un autre OS la configuration de l'active Directory

PCA = Plan continuité d'activité    }
PRA = Plan de reprise d'activité    } --> pour changer un disque à chaud

RAID5 = Minimum 3 disques




Crée un point de contrôle (Snapshot) quand tout est ajouté dans S1AD1 et S2AD2 avant de pouvoir ajouter d'autre service (DHCP, etc...)


iSCSI = Internet Small Computer System Interface






Ajouter un nouveau service S1AD1:
                          -> Services de fichiers et de stockage
                            -> Service de fichiers et iSCSI
                                -> ###coché### Serveur de fichier
                                -> Branchcache (une sorte de google drive)
                                -> ###coché### Déduplication de données (c'est un pointeur, on évite que les fichiers soient stoquer plusieurs fois, on ne copie que l'adresse)
                                -> ###coché### Dossier de travail
                                -> ###coché### Espaces de noms DFS (Distributed File System) c'est on va pouvoir faire de la synchronisation en temps réel
                                -> Fournisseur de stockage cible iSCSI
                                -> Gestionnaire de ressources serveur de fichier
                                -> ###coché### Réplication DFS (c'est ce qui permet de faire de la réplication DFS)
                                -> Serveur cible iSCSI
                                -> Serveur pour NFS
                                -> Service Agent VSS du serveur de fichiers
                            -> ###coché### Services de stockage

Suivant--> coché équilibreur de charge [NLB Network Loud Balancing (c'est quand on a un serveur qui a peter)] --> Suivant --> Installé --> Redémarrer

Faire la même chose sur S2AD2

sur S1AD1:
Outils --> Gestion de système de fichiers distribués DFS --> Espace de nom --> (à droite) Nouvel Espace de nom --> taper "S1AD1" (le trait sous le nom veux dire que c'est bien un objet de l'annuaire) --> Suivant --> Crée un espace qu'on appellera "DATA02" puis "Modifier les paramètres" --> Crée un dossier DATA02 sur D: --> OK --> Suivant --> Espace de noms de domaine --> Activer le mode Windows Server 2008 --> Suivant --> Crée


Sur S2AD2
Outils --> Gestion de système de fichiers distribués DFS --> Nouveau nom de réplication --> Groupe de réplication --> mettre S1AD1, S2AD2 --> Maille Pleine --> Bande Passante 1Mo --> S1AD1 Serveur principal --> prendre DATA02 (laisser autorisation) --> Definir un chemin (Modifier) --> Activer --> crée un chemin d'accès --> Crée
Re aller sur Réplication --> DATA01 --> clic droit Redupliquer maintenant


LUN (Logical Unit Network, Unité de réseaux logique) (partition mais en réseaux)


Baille DHCP = authorisation d'utilisation d'une adresse IP pour un temps donnée, au delas de la limite, il peut demander au serveur un renouvellement du bail



Ajouter un nouveau service S1AD1: Serveur DHCP --> Ajouter des fonctionnalités --> Suivant --> Suivant --> Suivant --> Installer --> lors de l'install cliquer sur "Terminer la configuration DHCP" et non "Terminer"
Check si c'est OK --> Suivant --> Suivant --> OK

Outils --> DHCP
Aller sur IPv4 --> clic droit, Nouvelle étendue --> Suivant --> Nom: etendu01 | Description: IP poste client --> Suivant --> Debut:10.33.10.5 | Fin:10.33.10.10 | Longueur: 24 | Masque: 255.255.255.0 --> (on configure ici les IP a exclures) Adresse debut 10.33.10.9 | Adresse fin: 10.33.10.9 -->
Suivant --> Bail de 10h seulement --> Adresse passerelle (ajouter les serveurs DNS ici S1AD1 et S2AD2) + 8.8.8.8 + 8.8.4.4 --> Suivant --> Ajouter dans Nom du serveur: S2AD2 --> Résoudre --> Ajouter --> Suivant --> WINS Ajouter S1AD1 --> Oui activer --> Suivant --> FINI


Si DNS oublier sur S2AD2:
Outils --> DHCP --> Option Étendu --> Serveur de noms

Pour acceder au backup:
C:\Windows\System32\dhcp\backup



Interopérable = S'intégré et s'adapter au milieu quand lequel on est.
Résilient = fonctionne avec tous le monde

WINS = Windows Internet Naming Service (Résouds des adresses NetBIOS)


GPO !

1 Validation règlement intérieur
2 Déploiement Firefox.msi
3 comment forcer page d'accueil sur Ynov.com
4 Supprimer le bouton "rechercher" (démon)
5 forcer un fond d'écran Ynov
6 interdire à l'utilisateur de supprimer/modifier le papier peint
7 installer calculette msi
8 création d'un répertoire individuel sur le réseaux %username%
9 déployer une imprimante avec ses drivers 32/64bits
10 quotas de disque à %username% à 50Mo

A FAIRE:
3 VMS: S1AD1 + S2AD2 + CLIENT
DFS : Réplication
IIS
DNS : Direct + inversé
Imprimante sur client
Imprimante internet
DHCP

Différence pro/enterprise:
Bitlocker


GPO Ordinateur est toujours supérieur au GPO Utilisateur
toujours la GPO la plus restrictive qui est prioritaire
l'OU est la plus petite instance

Pour implanté les GPO:
Gestionnaire de Serveur --> Outils --> Gestion et stratégie de Groupe
Clic droit --> Nouveau --> Unité d'organisation --> Crée 'COMPTA'
clic droit dans le vide --> Nouveau --> Utilisateur
Aller dans onglet "Computer" --> cliquer glisser vers "COMPTA"
Foret --> Domaine --> DOMAIN ("COMPTA" apparait) --> clic droit sur le nom --> Crée un objet GPO
sur l'objet crée --> clic droit modifié -->
ensuite, cela dépend ce que l'on veux:
[changer action de stratgie de groupe] Configuration Ordinateur --> Stratégie --> Paramètre Windows --> Paramètre de sécurité --> Stratégie de comptes --> Stratégie de mot de passe
[installer un logiciel] Configuration Ordinateur --> Stratégie --> Paramètre du logiciel --> Installation de logiciel --> clic droit Nouveau
[chose utile] Configuration Ordinateur --> Stratégie --> Modèle d'administration
[partage réseaux] Configuration Ordinateur --> Préférences --> Paramètre Windows --> Partage réseaux

[configuration fond d'écran du bureau] Configuration de l'utilisateur --> Stratégie --> Modèle d'administration --> Bureau --> Bureau


Pour appliquer le tout: Gestion de stratégie de groupe --> FORET DOMROUG --> Domaine --> DOMROUG.COM --> COMPTA --> 01 affichage --> clic droit sur COMPTA (menu droite) --> appliquer
CMD ==>
gpupdate /force # commande qui raffraichi les stratégies système et rafraischis les stratégie existante
gpresult /H toto.html # commande qui permet de voir si la stratégie s'est bien appliqué (afficher au format HTML)

pour voir si les MAJ ont été appliqué: Gestion de stratégie de groupe --> FORET DOMROUG --> Domaine --> DOMROUG.COM --> COMPTA --> 01 affichage --> Détails

POUR INFO: dans "Gestion et stratégie de groupe" :
Default Domain Policy = Tout le domaine
COMPTA = Tout le groupe compta


le : WMI --> Prépersonnalisation des images



Fichier des GPO:
"B2A-GPO-NOM-Prenom"

faire en style README.md au format WORD
Signature
