# TP 7 DBA - mariadb cluster

## docker-compose

```
version:  '3.7'

services:
  master:
    image: mariadb:10.4
    networks:
     - internal
    ports:
     - 3306
     - 4444
     - 4567
     - 4568
    restart: on-failure
    environment:
      MYSQL_ROOT_PASSWORD: password
    volumes:
     - ./master:/etc/mysql/mariadb.conf.d/
    command: '--wsrep-new-cluster'
  shard1:
    image: mariadb:10.4
    networks:
     - internal
    ports:
     - 3306
     - 4444
     - 4567
     - 4568
    restart: on-failure
    command: ''
    environment:
      MYSQL_ROOT_PASSWORD: password
    volumes:
     - ./shard1:/etc/mysql/mariadb.conf.d/
  shard2:
    image: mariadb:10.4
    networks:
      - internal
    ports:
      - 3306
      - 4444
      - 4567
      - 4568
    restart: on-failure
    environment:
      MYSQL_ROOT_PASSWORD: password
    volumes:
     - ./shard2:/etc/mysql/mariadb.conf.d/


networks:
  internal:
```
- création de 3 containers dockers avec une image ubuntu-mariadb tous sur un meme réseau avec les ports 3306, 4444, 4567 et 4568 ouverts
- dans chaque container il y a un volume monté contenant un fichier de configuration galera se trouvant dans `/etc/mysql/mariadb.conf.d/[nom_container].cnf`
- sur le premier node la commande l'entrypoint est modifié par `command:  "--wsrep-new-cluster"`

## fichiers de configuration galera

- master
    ```
    [mysqld]
    bind-address=0.0.0.0
    default_storage_engine=InnoDB
    binlog_format=row
    innodb_autoinc_lock_mode=2

    # Galera cluster configuration
    wsrep_on=ON
    wsrep_provider=/usr/lib/galera/libgalera_smm.so
    wsrep_cluster_address="gcomm://172.18.0.2,172.18.0.3,172.18.0.4"
    wsrep_cluster_name="mariadb-galera-cluster"
    wsrep_sst_method=rsync

    # Cluster node configuration
    wsrep_node_address="172.18.0.2"
    wsrep_node_name="master"
    ```
- shard1
    ```
    [mysqld]
    bind-address=0.0.0.0
    default_storage_engine=InnoDB
    binlog_format=row
    innodb_autoinc_lock_mode=2

    # Galera cluster configuration
    wsrep_on=ON
    wsrep_provider=/usr/lib/galera/libgalera_smm.so
    wsrep_cluster_address="gcomm://172.18.0.2,172.18.0.3,172.18.0.4"
    wsrep_cluster_name="shard-cluster"
    wsrep_sst_method=rsync

    # Cluster node configuration
    wsrep_node_address="172.18.0.3"
    wsrep_node_name="shard1"
    ```
- shard2
    ```
    [mysqld]
    bind-address=0.0.0.0
    default_storage_engine=InnoDB
    binlog_format=row
    innodb_autoinc_lock_mode=2

    # Galera cluster configuration
    wsrep_on=ON
    wsrep_provider=/usr/lib/galera/libgalera_smm.so
    wsrep_cluster_address="gcomm://172.18.0.2,172.18.0.3,172.18.0.4"
    wsrep_cluster_name="shard-cluster"
    wsrep_sst_method=rsync

    # Cluster node configuration
    wsrep_node_address="172.18.0.4"
    wsrep_node_name="shard2"
    ```
- la ligne `wsrep_cluster` liste chaque ip qui appartient a un server dans le cluster lui meme inclus

- `wsrep_node_address` spécifie l'ip du serveur