# TP 5

## mise en place des serveurs mariadb

### docker-compose.yml
```
version: '3.7'

services:
    master:
      environment: 
        MYSQL_ROOT_PASSWORD: password
      restart: on-failure
      image: mariadb:10.4
      networks: 
        - internal
      volumes:
        - ./maria:/var/lib/mysql
        - ./backups:/backups
        - ./configmaster:/etc/mysql/mariadb.conf.d/master.cnf
        - ./scripts:/scripts
      ports:
        - 3306:3306
    slave:
      environment: 
        MYSQL_ROOT_PASSWORD: password
      restart: on-failure
      image: mariadb:10.4
      networks:
        - internal
      volumes:
        - ./maria:/var/lib/mysql
        - ./backups:/backups
        - ./configslave:/etc/mysql/mariadb.conf.d/slave.cnf
        - ./scripts:/scripts
networks: 
  internal: 
```

## dossier scripts

- add-replicant.sh
```
#!/bin/bash

mysql -u root --password=password < /scripts/create-replicant.sql
```
- create-replicant.sql
```
CREATE USER 'slave_user'@'localhost' IDENTIFIED BY 'password';
GRANT REPLICATION SLAVE ON *.* TO 'slave_user'@'locahost';
FLUSH PRIVILIGES;
```
- unlock-tables.sh
```
#!/bin/bash
mysql -u root --password=password < /scripts/create-replicant.sql
```
- unlock-tables.sql
```
UNLOCK TABLES;
quit;
```
slaveconf.sql
```
slave stop;
CHANGE MASTER TO MASTER_HOST='172.22.114.33', MASTER_USER='slave_user', MASTER_PASSWORD='password', MASTER_LOG_FILE='mysql-bin.000003', MASTER_LOG_POS=11128001;
slave start;
show slave status\G;
```
slaveveconf.sh
```
#!/bin/bash
mysql -u root --password=password < /scripts/slaveconf.sql
```

## dossier configmaster
my.cnf
```
[mariadb]
server-id = 1
binlog-do-db=test
relay-log = /var/lib/mysql/mysql-relay-bin
relay-log-index = /var/lib/mysql/mysql-relay-bin.index
log-error = /var/lib/mysql/mysql.err
master-info-file = /var/lib/mysql/mysql-master.info
relay-log-info-file = /var/lib/mysql/mysql-relay-log.info
log-bin = /var/lib/mysql/mysql-bin
```

## dossier config slave
my.cnf
```
server-id = 2
master-host=172.22.114.33
master-connect-retry=60
master-user=slave_user
master-password=password
replicate-do-db=test
relay-log = /var/lib/mysql/mysql-relay-bin
relay-log-index = /var/lib/mysql/mysql-relay-bin.index
log-error = /var/lib/mysql/mysql.err
master-info-file = /var/lib/mysql/mysql-master.info
relay-log-info-file = /var/lib/mysql/mysql-relay-log.info
log-bin = /var/lib/mysql/mysql-bin
```

# mise en place du master-slave

## dans le master
- lancer la commande
` bash scripts/add-replicant.sh `

- faire un mysqldump
` mysqldump -u root -password=password --all-databases --master-data > /backups/dbdump.db`

- lancer la commande
` bash /scripts/unlock-tables.sh `

- copier le dump vers le slave
` scp /backups/dbdump.db root@172.22.115.57:/backups/dbdump.db`

# dans le slave

-  lancer la commande
`bash /scripts/slaveveconf.sh`

mysql -u root --password=password -e CREATE USER 'slave_user'@'localhost' IDENTIFIED BY 'password';