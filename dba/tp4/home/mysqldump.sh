#!/bin/bash


user="root"
password="password"
logPath="/var/log/backup"
backupName="dbBackup"   #$(date +%F)"
function Create_mysqldump()
{

    mkdir /backup
    if ["$?" = "0"]
    then
        echo "/backup dir created"
    else
        echo "/backup present"
    fi

    touch $logPath
    if ["$?" = "0"]
    then
        echo "logfile created"
    else
        echo "logfile present"
    fi


    mysqldump -u root --password=password --all-databases | gzip  > /backup/${backupName}.sql.gz
    if [ "$?" = "0" ]
	then

		echo "$(date +%D) backup created" | tee -a $logPath

	else
		echo "$(date +%D) mysqldump not created watchout" 1>&2 | tee -a $logPath 
		exit 1
	fi
}

#find "/backup" -name "dbBackup" -type - f -mtime "5" -exec rm -f {} \;

Create_mysqldump