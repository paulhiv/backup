CREATE TABLE public_events ( event_date DATE, event_name VARCHAR(255), event_age_requirement INT );
CREATE TABLE private_events LIKE public_events ;
INSERT INTO private_events SELECT * FROM public_events;

CREATE USER 'event_manager'@'localhost' IDENTIFIED BY 'password' ;
GRANT ALL PRIVILEGES ON events.private_events TO 'event_manager'@'localhost';
GRANT ALL PRIVILEGES ON events.public_events TO 'event_manager'@'localhost';
GRANT SELECT ON private_events TO 'event_manager'@'localhost';
GRANT SELECT ON public_events TO 'event_manager'@'localhost';
FLUSH PRIVILEGES;

CREATE USER 'event_supervisor'@'localhost' IDENTIFIED BY 'password' ;
GRANT SELECT ON public_events TO 'event_supervisor'@'localhost';
FLUSH PRIVILEGES;

use events;

INSERT INTO private_events (event_date, event_name, event_age_requirement)
VALUES ( 2001-01-21, bateau, 22);

INSERT INTO prublic_events (event_date, event_name, event_age_requirement)
VALUES ( '2008-8-21', 'desert-rock', '22' );

DROP USER 'event_supervisor'@'localhost';

ERROR 1142 (42000): SELECT command denied to user 'event_supervisor'@'localhost' for table 'private_events'

select user from mysql.user;

