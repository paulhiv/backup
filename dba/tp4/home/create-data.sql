CREATE DATABASE teams ;

USE teams ; 

CREATE TABLE games
( match_date DATE, victory VARCHAR(255), observations VARCHAR(255) );

CREATE TABLE players
( firstname VARCHAR(100), lastname VARCHAR(100), start_date DATE );

CREATE USER 'manager'@'localhost' IDENTIFIED BY 'manager_password' ;
GRANT ALL PRIVILEGES ON teams.games TO 'manager'@'localhost';
FLUSH PRIVILEGES;

CREATE USER 'recruiter'@'localhost' IDENTIFIED BY 'recruiter_password' ;
GRANT INSERT, SELECT, UPDATE ON teams.players TO 'manager'@'localhost';
FLUSH PRIVILEGES;
