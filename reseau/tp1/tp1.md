# infos générales

batiment
- 200 Utilisateurs et 3 serveurs physiques (50VMs) sur le premier bâtiment.
- 50 Utilisateurs et 1 serveur physique sur le second bâtiment.
- 100 Utilisateurs et 1 serveur physique sur le troisième bâtiment. 

VLANS
- Marketing
- Finance
- SI
- serveurs

ip courrantes
- Bât 1 :  - 192.168.1.0/24
- Bât 2 :  - 192.168.2.0/24
- Bât 3 :  - 192.168.3.0/24
 
# infos a rendre avec le schema 

architechture générale
- routage statique
   [cf pas de ospf sinon on rajoute des routeurs](https://support.zyxel.eu/hc/article_attachments/360001612614/Policy_Routing_Walkthrough.pdf)
- vlan par switch (demander si des départements partagent des bureaux)
- 4 VLANS


infos techinques
    - options en fonction du budget
        - ospf/backbone
        - rajouter des swiths pour plus d'utilisateurs

batiment 1
    - 10 switchs access
    - salle server
        - 2 switchs
        - 3 serveurs
        - 2 firewalls
        - 1 routeur MPLS
192.168.1.0/24


batiment 2
    - 5 switchs
    - 1 serveur
    - 2 firewall
    - 1 routeur MPLS
192.168.2.0/26


batiement 3
    - 6 switchs
    salle serveur
        - 2 switchs
        - 1 serveur
        - 1 firewall
192.168.3.0/25