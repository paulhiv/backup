## présentation du projet

mise en place de deux serveurs tse dockerisé avec un broker tse, contenant un firewall, vpn ssl et une dmz.

## technologies utilisées

- guacamole
- pfsense
- esxi
- vmware
- windows server 2016 AD et ferme RDS
- centos


## topologie
```
|----------------|                |-------------|
|                |                |             |
| guacamole 1    |-------| |------| guacamole 2 |
|                |       | |      |             |
|----------------|       | |      |-------------|
                         | |
                    |------------|
                    |            |
        |-----------|   pfsense  |--------------|
        |           |            |              |
        |           |------------|              |
        |               |     |                 |
        |               |     |                 |
        |               |     |                 |
|------------------|    |     |     |---------------------|
|                  |    |     |     |                     |
|windows server AD |    |     |     | windows server RDS  |
|                  |    |     |     |                     |
|------------------|    |     |     |---------------------|
                        |     |
                        |     |
|------------------|    |     |     |-------------------|
|   client centos  |-----     ------|   client windows  |
|------------------|                |-------------------|
```


