## Git global setup
```
git config --global user.name "paul.hivert@ynov.com"
git config --global user.email "paul.hivert"
```

## Create a new repository
```
git clone {git url}
cd rendu-tp-node
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```
## Existing folder
```
cd existing_folder
git init
git remote add origin {git url}
git add .
git commit -m "Initial commit"
git push -u origin master
```

## Existing Git repository
```
cd existing_repo
git remote rename origin old-origin
git remote add origin {git url}
git push -u origin --all
git push -u origin --tags
```
